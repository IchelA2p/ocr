﻿namespace PDFEXTRACT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.dtf = new System.Windows.Forms.DataGridView();
            this.btnGetInv = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Parse = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDisconFee = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtLateFee = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtPrevBal = new System.Windows.Forms.TextBox();
            this.txtPaymentRcvd = new System.Windows.Forms.TextBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.txtCurrCharge = new System.Windows.Forms.TextBox();
            this.txtTotalAmt = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chkTransAmtNo = new System.Windows.Forms.CheckBox();
            this.chkTransAmtYes = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.dtDueDate = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.dtSvcFr = new System.Windows.Forms.DateTimePicker();
            this.dtBillDate = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.dtSvcTo = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.chbFinalBillNo = new System.Windows.Forms.CheckBox();
            this.label42 = new System.Windows.Forms.Label();
            this.chbFinalBillYes = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHouseNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtZipcode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.txtUSPCity = new System.Windows.Forms.TextBox();
            this.txtUSPState = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtUSPPOBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUSPStreet = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUSPZipcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmboBx_USP = new System.Windows.Forms.ComboBox();
            this.lbl_UspOnInvoice = new System.Windows.Forms.Label();
            this.cmbBillName = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAcctNo = new System.Windows.Forms.TextBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbOtherUtility = new System.Windows.Forms.ComboBox();
            this.chkOthers = new System.Windows.Forms.CheckBox();
            this.chkWater = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkGas = new System.Windows.Forms.CheckBox();
            this.chkElec = new System.Windows.Forms.CheckBox();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.act_utility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.billdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.servicefrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prevamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paidamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.act_currcharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calc_currcharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latefee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disconnectionfee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deposit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.credit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.interest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.other_charges = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountdue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calc_amountdue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalbill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.svcaddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.svcaddresscity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.svcaddressstate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.svcaddresszipcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.property_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtf)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.richTextBox1);
            this.splitContainer1.Panel1.Controls.Add(this.dtf);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetInv);
            this.splitContainer1.Panel1.Controls.Add(this.webBrowser1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(158, 140);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 0;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(4, 393);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(724, 313);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.Visible = false;
            // 
            // dtf
            // 
            this.dtf.AllowUserToDeleteRows = false;
            this.dtf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.act_utility,
            this.accountno,
            this.billdate,
            this.servicefrom,
            this.serviceto,
            this.duedate,
            this.prevamount,
            this.paidamount,
            this.act_currcharge,
            this.calc_currcharge,
            this.latefee,
            this.disconnectionfee,
            this.deposit,
            this.credit,
            this.interest,
            this.other_charges,
            this.amountdue,
            this.calc_amountdue,
            this.finalbill,
            this.svcaddress,
            this.svcaddresscity,
            this.svcaddressstate,
            this.svcaddresszipcode,
            this.property_code});
            this.dtf.Location = new System.Drawing.Point(-1, -958);
            this.dtf.Name = "dtf";
            this.dtf.Size = new System.Drawing.Size(0, 97);
            this.dtf.TabIndex = 0;
            // 
            // btnGetInv
            // 
            this.btnGetInv.BackColor = System.Drawing.Color.SteelBlue;
            this.btnGetInv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetInv.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetInv.ForeColor = System.Drawing.Color.White;
            this.btnGetInv.Location = new System.Drawing.Point(8, 6);
            this.btnGetInv.Name = "btnGetInv";
            this.btnGetInv.Size = new System.Drawing.Size(86, 28);
            this.btnGetInv.TabIndex = 7;
            this.btnGetInv.Text = "Get Invoice";
            this.btnGetInv.UseVisualStyleBackColor = false;
            this.btnGetInv.Click += new System.EventHandler(this.btnGetInv_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(3, 40);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(20, 20);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Parse);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.pictureBox26);
            this.panel1.Controls.Add(this.pictureBox25);
            this.panel1.Controls.Add(this.pictureBox22);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.groupBox10);
            this.panel1.Controls.Add(this.groupBox9);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox8);
            this.panel1.Controls.Add(this.cmboBx_USP);
            this.panel1.Controls.Add(this.lbl_UspOnInvoice);
            this.panel1.Controls.Add(this.cmbBillName);
            this.panel1.Controls.Add(this.label61);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtAcctNo);
            this.panel1.Controls.Add(this.checkBox6);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(127, 138);
            this.panel1.TabIndex = 8;
            // 
            // Parse
            // 
            this.Parse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Parse.Location = new System.Drawing.Point(-455, 731);
            this.Parse.Name = "Parse";
            this.Parse.Size = new System.Drawing.Size(75, 29);
            this.Parse.TabIndex = 1;
            this.Parse.Text = "Parse";
            this.Parse.UseVisualStyleBackColor = true;
            this.Parse.Visible = false;
            this.Parse.Click += new System.EventHandler(this.Parse_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.SteelBlue;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(-610, -900);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 26);
            this.btnSave.TabIndex = 236;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox26.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox26.Image")));
            this.pictureBox26.Location = new System.Drawing.Point(541, 308);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(19, 20);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 154;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Visible = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox25.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox25.Image")));
            this.pictureBox25.Location = new System.Drawing.Point(516, 308);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(19, 20);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 153;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Visible = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox22.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox22.Image")));
            this.pictureBox22.Location = new System.Drawing.Point(630, 151);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(19, 20);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 154;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(605, 151);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 20);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 153;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(409, 118);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 154;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(384, 118);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 153;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Controls.Add(this.label13);
            this.groupBox10.Controls.Add(this.txtDisconFee);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.pictureBox34);
            this.groupBox10.Controls.Add(this.pictureBox13);
            this.groupBox10.Controls.Add(this.pictureBox12);
            this.groupBox10.Controls.Add(this.pictureBox11);
            this.groupBox10.Controls.Add(this.pictureBox33);
            this.groupBox10.Controls.Add(this.pictureBox32);
            this.groupBox10.Controls.Add(this.pictureBox31);
            this.groupBox10.Controls.Add(this.pictureBox21);
            this.groupBox10.Controls.Add(this.pictureBox20);
            this.groupBox10.Controls.Add(this.pictureBox19);
            this.groupBox10.Controls.Add(this.pictureBox18);
            this.groupBox10.Controls.Add(this.pictureBox17);
            this.groupBox10.Controls.Add(this.pictureBox16);
            this.groupBox10.Controls.Add(this.pictureBox10);
            this.groupBox10.Controls.Add(this.pictureBox9);
            this.groupBox10.Controls.Add(this.pictureBox8);
            this.groupBox10.Controls.Add(this.txtLateFee);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.label44);
            this.groupBox10.Controls.Add(this.label45);
            this.groupBox10.Controls.Add(this.label47);
            this.groupBox10.Controls.Add(this.txtPrevBal);
            this.groupBox10.Controls.Add(this.txtPaymentRcvd);
            this.groupBox10.Controls.Add(this.checkBox35);
            this.groupBox10.Controls.Add(this.label48);
            this.groupBox10.Controls.Add(this.textBox22);
            this.groupBox10.Controls.Add(this.txtCurrCharge);
            this.groupBox10.Controls.Add(this.txtTotalAmt);
            this.groupBox10.Controls.Add(this.label49);
            this.groupBox10.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.groupBox10.Location = new System.Drawing.Point(387, 463);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(0, 273);
            this.groupBox10.TabIndex = 235;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Payment Breakdown";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(176, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 14);
            this.label12.TabIndex = 211;
            this.label12.Text = "$";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(176, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 14);
            this.label13.TabIndex = 210;
            this.label13.Text = "$";
            // 
            // txtDisconFee
            // 
            this.txtDisconFee.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisconFee.Location = new System.Drawing.Point(192, 142);
            this.txtDisconFee.Name = "txtDisconFee";
            this.txtDisconFee.Size = new System.Drawing.Size(106, 22);
            this.txtDisconFee.TabIndex = 204;
            this.txtDisconFee.Text = "0.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(176, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 14);
            this.label15.TabIndex = 205;
            this.label15.Text = "$";
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox34.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox34.Image")));
            this.pictureBox34.Location = new System.Drawing.Point(322, 224);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(19, 20);
            this.pictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox34.TabIndex = 154;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Visible = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(307, 224);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(19, 20);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 153;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Visible = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(322, 196);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(19, 20);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 154;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Visible = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(307, 196);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(19, 20);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 153;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Visible = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox33.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox33.Image")));
            this.pictureBox33.Location = new System.Drawing.Point(322, 169);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(19, 20);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox33.TabIndex = 154;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Visible = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox32.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox32.Image")));
            this.pictureBox32.Location = new System.Drawing.Point(307, 169);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(19, 20);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox32.TabIndex = 153;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Visible = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox31.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox31.Image")));
            this.pictureBox31.Location = new System.Drawing.Point(322, 140);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(19, 20);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox31.TabIndex = 154;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.Visible = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(307, 140);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(19, 20);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 153;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Visible = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(322, 114);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(19, 20);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 154;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(307, 114);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(19, 20);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 153;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Visible = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(322, 85);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(19, 20);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 154;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Visible = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(307, 85);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(19, 20);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 153;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Visible = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(322, 58);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(19, 20);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 154;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Visible = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(307, 58);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(19, 20);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 153;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(322, 27);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(19, 20);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 154;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Visible = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(307, 27);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(19, 20);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 153;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Visible = false;
            // 
            // txtLateFee
            // 
            this.txtLateFee.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLateFee.Location = new System.Drawing.Point(192, 114);
            this.txtLateFee.Name = "txtLateFee";
            this.txtLateFee.Size = new System.Drawing.Size(106, 22);
            this.txtLateFee.TabIndex = 183;
            this.txtLateFee.Text = "0.00";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(176, 118);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 14);
            this.label16.TabIndex = 184;
            this.label16.Text = "$";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(20, 146);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(110, 14);
            this.label21.TabIndex = 103;
            this.label21.Text = "Disconnection Fee:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(20, 119);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 14);
            this.label20.TabIndex = 103;
            this.label20.Text = "Late fee:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(20, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 14);
            this.label19.TabIndex = 103;
            this.label19.Text = "Current charge:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(20, 62);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(105, 14);
            this.label18.TabIndex = 103;
            this.label18.Text = "Payment received:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(20, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 14);
            this.label17.TabIndex = 103;
            this.label17.Text = "Previous balance:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(20, 173);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(156, 14);
            this.label44.TabIndex = 103;
            this.label44.Text = "TOTAL AMOUNT ON INVOICE:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(20, 201);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(156, 14);
            this.label45.TabIndex = 106;
            this.label45.Text = "TOTAL AMT AFTER DUE DATE:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(176, 27);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(13, 14);
            this.label47.TabIndex = 93;
            this.label47.Text = "$";
            // 
            // txtPrevBal
            // 
            this.txtPrevBal.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrevBal.Location = new System.Drawing.Point(192, 27);
            this.txtPrevBal.Name = "txtPrevBal";
            this.txtPrevBal.Size = new System.Drawing.Size(106, 22);
            this.txtPrevBal.TabIndex = 24;
            this.txtPrevBal.Text = "0.00";
            // 
            // txtPaymentRcvd
            // 
            this.txtPaymentRcvd.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentRcvd.Location = new System.Drawing.Point(192, 58);
            this.txtPaymentRcvd.Name = "txtPaymentRcvd";
            this.txtPaymentRcvd.Size = new System.Drawing.Size(106, 22);
            this.txtPaymentRcvd.TabIndex = 27;
            this.txtPaymentRcvd.Text = "0.00";
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox35.ForeColor = System.Drawing.Color.Black;
            this.checkBox35.Location = new System.Drawing.Point(192, 231);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(85, 18);
            this.checkBox35.TabIndex = 25;
            this.checkBox35.Text = "no amount";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(176, 61);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 14);
            this.label48.TabIndex = 97;
            this.label48.Text = "$";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.Location = new System.Drawing.Point(192, 198);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(106, 22);
            this.textBox22.TabIndex = 33;
            // 
            // txtCurrCharge
            // 
            this.txtCurrCharge.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrCharge.Location = new System.Drawing.Point(192, 85);
            this.txtCurrCharge.Name = "txtCurrCharge";
            this.txtCurrCharge.Size = new System.Drawing.Size(106, 22);
            this.txtCurrCharge.TabIndex = 98;
            this.txtCurrCharge.Text = "0.00";
            // 
            // txtTotalAmt
            // 
            this.txtTotalAmt.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalAmt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmt.Location = new System.Drawing.Point(192, 169);
            this.txtTotalAmt.Name = "txtTotalAmt";
            this.txtTotalAmt.Size = new System.Drawing.Size(106, 22);
            this.txtTotalAmt.TabIndex = 32;
            this.txtTotalAmt.Text = "0.00";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(176, 90);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 14);
            this.label49.TabIndex = 99;
            this.label49.Text = "$";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.pictureBox47);
            this.groupBox9.Controls.Add(this.pictureBox46);
            this.groupBox9.Controls.Add(this.pictureBox45);
            this.groupBox9.Controls.Add(this.pictureBox44);
            this.groupBox9.Controls.Add(this.pictureBox39);
            this.groupBox9.Controls.Add(this.pictureBox37);
            this.groupBox9.Controls.Add(this.pictureBox15);
            this.groupBox9.Controls.Add(this.pictureBox14);
            this.groupBox9.Controls.Add(this.pictureBox36);
            this.groupBox9.Controls.Add(this.pictureBox35);
            this.groupBox9.Controls.Add(this.pictureBox7);
            this.groupBox9.Controls.Add(this.pictureBox6);
            this.groupBox9.Controls.Add(this.pictureBox2);
            this.groupBox9.Controls.Add(this.pictureBox1);
            this.groupBox9.Controls.Add(this.chkTransAmtNo);
            this.groupBox9.Controls.Add(this.chkTransAmtYes);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this.checkBox27);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.dateTimePicker6);
            this.groupBox9.Controls.Add(this.checkBox28);
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.checkBox29);
            this.groupBox9.Controls.Add(this.label32);
            this.groupBox9.Controls.Add(this.checkBox30);
            this.groupBox9.Controls.Add(this.checkBox31);
            this.groupBox9.Controls.Add(this.textBox18);
            this.groupBox9.Controls.Add(this.checkBox32);
            this.groupBox9.Controls.Add(this.dtDueDate);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.dtSvcFr);
            this.groupBox9.Controls.Add(this.dtBillDate);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.dtSvcTo);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.chbFinalBillNo);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.chbFinalBillYes);
            this.groupBox9.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.groupBox9.Location = new System.Drawing.Point(15, 464);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(366, 273);
            this.groupBox9.TabIndex = 234;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Billing Information";
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox47.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox47.Image")));
            this.pictureBox47.Location = new System.Drawing.Point(327, 243);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(19, 20);
            this.pictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox47.TabIndex = 154;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Visible = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox46.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox46.Image")));
            this.pictureBox46.Location = new System.Drawing.Point(302, 243);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(19, 20);
            this.pictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox46.TabIndex = 153;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Visible = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox45.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox45.Image")));
            this.pictureBox45.Location = new System.Drawing.Point(372, 196);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(19, 20);
            this.pictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox45.TabIndex = 154;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Visible = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox44.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox44.Image")));
            this.pictureBox44.Location = new System.Drawing.Point(357, 196);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(19, 20);
            this.pictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox44.TabIndex = 153;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Visible = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox39.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox39.Image")));
            this.pictureBox39.Location = new System.Drawing.Point(372, 168);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(19, 20);
            this.pictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox39.TabIndex = 154;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.Visible = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox37.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox37.Image")));
            this.pictureBox37.Location = new System.Drawing.Point(357, 168);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(19, 20);
            this.pictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox37.TabIndex = 153;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(372, 135);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(19, 20);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 154;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Visible = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(357, 135);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(19, 20);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 153;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Visible = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox36.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox36.Image")));
            this.pictureBox36.Location = new System.Drawing.Point(357, 77);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(19, 20);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox36.TabIndex = 154;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Visible = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox35.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox35.Image")));
            this.pictureBox35.Location = new System.Drawing.Point(332, 77);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(19, 20);
            this.pictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox35.TabIndex = 153;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Visible = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(357, 50);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(19, 20);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 154;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(332, 50);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(19, 20);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 153;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(357, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 154;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(332, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 153;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // chkTransAmtNo
            // 
            this.chkTransAmtNo.AutoSize = true;
            this.chkTransAmtNo.Checked = true;
            this.chkTransAmtNo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTransAmtNo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkTransAmtNo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTransAmtNo.ForeColor = System.Drawing.Color.Black;
            this.chkTransAmtNo.Location = new System.Drawing.Point(243, 247);
            this.chkTransAmtNo.Name = "chkTransAmtNo";
            this.chkTransAmtNo.Size = new System.Drawing.Size(41, 18);
            this.chkTransAmtNo.TabIndex = 213;
            this.chkTransAmtNo.Text = "No";
            this.chkTransAmtNo.UseVisualStyleBackColor = true;
            this.chkTransAmtNo.Visible = false;
            // 
            // chkTransAmtYes
            // 
            this.chkTransAmtYes.AutoSize = true;
            this.chkTransAmtYes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkTransAmtYes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTransAmtYes.ForeColor = System.Drawing.Color.Black;
            this.chkTransAmtYes.Location = new System.Drawing.Point(197, 247);
            this.chkTransAmtYes.Name = "chkTransAmtYes";
            this.chkTransAmtYes.Size = new System.Drawing.Size(44, 18);
            this.chkTransAmtYes.TabIndex = 214;
            this.chkTransAmtYes.Text = "Yes";
            this.chkTransAmtYes.UseVisualStyleBackColor = true;
            this.chkTransAmtYes.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(20, 247);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(158, 14);
            this.label58.TabIndex = 212;
            this.label58.Text = "Is there a Balance transfer?";
            this.label58.Visible = false;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox27.ForeColor = System.Drawing.Color.Black;
            this.checkBox27.Location = new System.Drawing.Point(193, 140);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(44, 18);
            this.checkBox27.TabIndex = 129;
            this.checkBox27.Text = "Yes";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(20, 141);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(169, 14);
            this.label29.TabIndex = 128;
            this.label29.Text = "Is there a disconnection date:";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.Location = new System.Drawing.Point(243, 137);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(97, 22);
            this.dateTimePicker6.TabIndex = 127;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Checked = true;
            this.checkBox28.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox28.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox28.ForeColor = System.Drawing.Color.Black;
            this.checkBox28.Location = new System.Drawing.Point(306, 168);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(41, 18);
            this.checkBox28.TabIndex = 84;
            this.checkBox28.Text = "No";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(162, 223);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(59, 14);
            this.label41.TabIndex = 132;
            this.label41.Text = "Deposit $";
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox29.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox29.ForeColor = System.Drawing.Color.Black;
            this.checkBox29.Location = new System.Drawing.Point(266, 168);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(44, 18);
            this.checkBox29.TabIndex = 83;
            this.checkBox29.Text = "Yes";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(20, 169);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(256, 14);
            this.label32.TabIndex = 82;
            this.label32.Text = "Is there a comment that says \"DO NOT MAIL\" ?";
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox30.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox30.ForeColor = System.Drawing.Color.Black;
            this.checkBox30.Location = new System.Drawing.Point(205, 27);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(112, 18);
            this.checkBox30.TabIndex = 81;
            this.checkBox30.Text = "No Invoice Date";
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox31.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox31.ForeColor = System.Drawing.Color.Black;
            this.checkBox31.Location = new System.Drawing.Point(206, 82);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(110, 18);
            this.checkBox31.TabIndex = 80;
            this.checkBox31.Text = "No Billing Cycle";
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(227, 220);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(41, 22);
            this.textBox18.TabIndex = 131;
            this.textBox18.Text = "0";
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox32.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox32.ForeColor = System.Drawing.Color.Black;
            this.checkBox32.Location = new System.Drawing.Point(206, 55);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(95, 18);
            this.checkBox32.TabIndex = 79;
            this.checkBox32.Text = "No Due Date";
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // dtDueDate
            // 
            this.dtDueDate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDueDate.Location = new System.Drawing.Point(102, 51);
            this.dtDueDate.Name = "dtDueDate";
            this.dtDueDate.Size = new System.Drawing.Size(96, 22);
            this.dtDueDate.TabIndex = 20;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(20, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 14);
            this.label35.TabIndex = 4;
            this.label35.Text = "Due Date:";
            // 
            // dtSvcFr
            // 
            this.dtSvcFr.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSvcFr.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtSvcFr.Location = new System.Drawing.Point(102, 79);
            this.dtSvcFr.Name = "dtSvcFr";
            this.dtSvcFr.Size = new System.Drawing.Size(97, 22);
            this.dtSvcFr.TabIndex = 21;
            // 
            // dtBillDate
            // 
            this.dtBillDate.CalendarForeColor = System.Drawing.Color.Black;
            this.dtBillDate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtBillDate.Location = new System.Drawing.Point(101, 24);
            this.dtBillDate.Name = "dtBillDate";
            this.dtBillDate.Size = new System.Drawing.Size(97, 22);
            this.dtBillDate.TabIndex = 19;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(20, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 14);
            this.label36.TabIndex = 78;
            this.label36.Text = "Invoice Date:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(20, 83);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(78, 14);
            this.label37.TabIndex = 27;
            this.label37.Text = "Service From:";
            // 
            // dtSvcTo
            // 
            this.dtSvcTo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSvcTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtSvcTo.Location = new System.Drawing.Point(102, 105);
            this.dtSvcTo.Name = "dtSvcTo";
            this.dtSvcTo.Size = new System.Drawing.Size(97, 22);
            this.dtSvcTo.TabIndex = 22;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(20, 111);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(63, 14);
            this.label40.TabIndex = 29;
            this.label40.Text = "Service To:";
            // 
            // chbFinalBillNo
            // 
            this.chbFinalBillNo.AutoSize = true;
            this.chbFinalBillNo.Checked = true;
            this.chbFinalBillNo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbFinalBillNo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chbFinalBillNo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbFinalBillNo.ForeColor = System.Drawing.Color.Black;
            this.chbFinalBillNo.Location = new System.Drawing.Point(306, 196);
            this.chbFinalBillNo.Name = "chbFinalBillNo";
            this.chbFinalBillNo.Size = new System.Drawing.Size(41, 18);
            this.chbFinalBillNo.TabIndex = 130;
            this.chbFinalBillNo.Text = "No";
            this.chbFinalBillNo.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(19, 196);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(237, 14);
            this.label42.TabIndex = 112;
            this.label42.Text = "Is there a comment that says \"FINAL BILL\"?";
            // 
            // chbFinalBillYes
            // 
            this.chbFinalBillYes.AutoSize = true;
            this.chbFinalBillYes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chbFinalBillYes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbFinalBillYes.ForeColor = System.Drawing.Color.Black;
            this.chbFinalBillYes.Location = new System.Drawing.Point(266, 196);
            this.chbFinalBillYes.Name = "chbFinalBillYes";
            this.chbFinalBillYes.Size = new System.Drawing.Size(44, 18);
            this.chbFinalBillYes.TabIndex = 130;
            this.chbFinalBillYes.Text = "Yes";
            this.chbFinalBillYes.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pictureBox30);
            this.groupBox1.Controls.Add(this.pictureBox29);
            this.groupBox1.Controls.Add(this.txtCity);
            this.groupBox1.Controls.Add(this.txtState);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtHouseNo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtStreet);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtZipcode);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(17, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(0, 110);
            this.groupBox1.TabIndex = 233;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Service Address";
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox30.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox30.Image")));
            this.pictureBox30.Location = new System.Drawing.Point(514, 18);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(19, 20);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox30.TabIndex = 154;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.Visible = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox29.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox29.Image")));
            this.pictureBox29.Location = new System.Drawing.Point(489, 18);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(19, 20);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 153;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.Visible = false;
            // 
            // txtCity
            // 
            this.txtCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCity.Location = new System.Drawing.Point(79, 50);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(307, 22);
            this.txtCity.TabIndex = 123;
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(443, 50);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(39, 22);
            this.txtState.TabIndex = 122;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "House #:";
            // 
            // txtHouseNo
            // 
            this.txtHouseNo.Location = new System.Drawing.Point(79, 22);
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Size = new System.Drawing.Size(54, 22);
            this.txtHouseNo.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Street:";
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(221, 22);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(259, 22);
            this.txtStreet.TabIndex = 8;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox1.Location = new System.Drawing.Point(137, 80);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(87, 18);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "No Zipcode";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "City:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(396, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 14);
            this.label9.TabIndex = 14;
            this.label9.Text = "State:";
            // 
            // txtZipcode
            // 
            this.txtZipcode.Location = new System.Drawing.Point(79, 78);
            this.txtZipcode.MaxLength = 5;
            this.txtZipcode.Name = "txtZipcode";
            this.txtZipcode.Size = new System.Drawing.Size(54, 22);
            this.txtZipcode.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Zipcode:";
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.pictureBox28);
            this.groupBox8.Controls.Add(this.pictureBox27);
            this.groupBox8.Controls.Add(this.txtUSPCity);
            this.groupBox8.Controls.Add(this.txtUSPState);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.txtUSPPOBox);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.txtUSPStreet);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.txtUSPZipcode);
            this.groupBox8.Controls.Add(this.label1);
            this.groupBox8.Location = new System.Drawing.Point(17, 340);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(0, 110);
            this.groupBox8.TabIndex = 232;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "USP Address";
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox28.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox28.Image")));
            this.pictureBox28.Location = new System.Drawing.Point(515, 22);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(19, 20);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox28.TabIndex = 154;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.Visible = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox27.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox27.Image")));
            this.pictureBox27.Location = new System.Drawing.Point(490, 22);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(19, 20);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 153;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Visible = false;
            // 
            // txtUSPCity
            // 
            this.txtUSPCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtUSPCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtUSPCity.Location = new System.Drawing.Point(78, 50);
            this.txtUSPCity.Name = "txtUSPCity";
            this.txtUSPCity.Size = new System.Drawing.Size(306, 22);
            this.txtUSPCity.TabIndex = 123;
            // 
            // txtUSPState
            // 
            this.txtUSPState.Location = new System.Drawing.Point(443, 50);
            this.txtUSPState.MaxLength = 2;
            this.txtUSPState.Name = "txtUSPState";
            this.txtUSPState.Size = new System.Drawing.Size(39, 22);
            this.txtUSPState.TabIndex = 122;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(21, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 14);
            this.label26.TabIndex = 19;
            this.label26.Text = "PO Box:";
            // 
            // txtUSPPOBox
            // 
            this.txtUSPPOBox.Location = new System.Drawing.Point(78, 22);
            this.txtUSPPOBox.Name = "txtUSPPOBox";
            this.txtUSPPOBox.Size = new System.Drawing.Size(81, 22);
            this.txtUSPPOBox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(165, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Street:";
            // 
            // txtUSPStreet
            // 
            this.txtUSPStreet.Location = new System.Drawing.Point(209, 22);
            this.txtUSPStreet.Name = "txtUSPStreet";
            this.txtUSPStreet.Size = new System.Drawing.Size(271, 22);
            this.txtUSPStreet.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 14);
            this.label7.TabIndex = 16;
            this.label7.Text = "City:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(399, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "State:";
            // 
            // txtUSPZipcode
            // 
            this.txtUSPZipcode.Location = new System.Drawing.Point(79, 78);
            this.txtUSPZipcode.MaxLength = 5;
            this.txtUSPZipcode.Name = "txtUSPZipcode";
            this.txtUSPZipcode.Size = new System.Drawing.Size(54, 22);
            this.txtUSPZipcode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zipcode:";
            // 
            // cmboBx_USP
            // 
            this.cmboBx_USP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmboBx_USP.FormattingEnabled = true;
            this.cmboBx_USP.Location = new System.Drawing.Point(122, 308);
            this.cmboBx_USP.Name = "cmboBx_USP";
            this.cmboBx_USP.Size = new System.Drawing.Size(375, 22);
            this.cmboBx_USP.TabIndex = 227;
            // 
            // lbl_UspOnInvoice
            // 
            this.lbl_UspOnInvoice.AutoSize = true;
            this.lbl_UspOnInvoice.Location = new System.Drawing.Point(18, 311);
            this.lbl_UspOnInvoice.Name = "lbl_UspOnInvoice";
            this.lbl_UspOnInvoice.Size = new System.Drawing.Size(89, 14);
            this.lbl_UspOnInvoice.TabIndex = 226;
            this.lbl_UspOnInvoice.Text = "USP on Invoice:";
            // 
            // cmbBillName
            // 
            this.cmbBillName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbBillName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBillName.FormattingEnabled = true;
            this.cmbBillName.Location = new System.Drawing.Point(122, 117);
            this.cmbBillName.Name = "cmbBillName";
            this.cmbBillName.Size = new System.Drawing.Size(256, 22);
            this.cmbBillName.TabIndex = 225;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(19, 121);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(104, 14);
            this.label61.TabIndex = 223;
            this.label61.Text = "Bill addressed to:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(19, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 14);
            this.label14.TabIndex = 208;
            this.label14.Text = "Account Number:";
            // 
            // txtAcctNo
            // 
            this.txtAcctNo.BackColor = System.Drawing.Color.White;
            this.txtAcctNo.ForeColor = System.Drawing.Color.Black;
            this.txtAcctNo.Location = new System.Drawing.Point(122, 149);
            this.txtAcctNo.MaxLength = 25;
            this.txtAcctNo.Name = "txtAcctNo";
            this.txtAcctNo.Size = new System.Drawing.Size(256, 22);
            this.txtAcctNo.TabIndex = 206;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox6.Location = new System.Drawing.Point(389, 151);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(189, 18);
            this.checkBox6.TabIndex = 207;
            this.checkBox6.Text = "no account number on invoice";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox23);
            this.groupBox2.Controls.Add(this.pictureBox24);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cmbOtherUtility);
            this.groupBox2.Controls.Add(this.chkOthers);
            this.groupBox2.Controls.Add(this.chkWater);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.chkGas);
            this.groupBox2.Controls.Add(this.chkElec);
            this.groupBox2.Location = new System.Drawing.Point(17, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(0, 72);
            this.groupBox2.TabIndex = 205;
            this.groupBox2.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox23.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox23.Image")));
            this.pictureBox23.Location = new System.Drawing.Point(512, 25);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(19, 20);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 152;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Visible = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox24.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox24.Image")));
            this.pictureBox24.Location = new System.Drawing.Point(487, 25);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(19, 20);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 151;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 14);
            this.label8.TabIndex = 9;
            this.label8.Text = "Please specify the type of Bill";
            // 
            // cmbOtherUtility
            // 
            this.cmbOtherUtility.BackColor = System.Drawing.Color.White;
            this.cmbOtherUtility.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOtherUtility.FormattingEnabled = true;
            this.cmbOtherUtility.Items.AddRange(new object[] {
            "Garbage/Trash/Disposal",
            "Stormwater/Drainage/WasteWater/Sewer",
            "TaxBill/TaxSale/property bill",
            "Cut Grass/weeds removal/Lawn maintenance",
            "Removal of debris",
            "Association/assessment fee",
            "Septic",
            "Others (Non-Billing Information)"});
            this.cmbOtherUtility.Location = new System.Drawing.Point(221, 40);
            this.cmbOtherUtility.Name = "cmbOtherUtility";
            this.cmbOtherUtility.Size = new System.Drawing.Size(259, 22);
            this.cmbOtherUtility.TabIndex = 8;
            // 
            // chkOthers
            // 
            this.chkOthers.AutoSize = true;
            this.chkOthers.Location = new System.Drawing.Point(414, 16);
            this.chkOthers.Name = "chkOthers";
            this.chkOthers.Size = new System.Drawing.Size(62, 18);
            this.chkOthers.TabIndex = 7;
            this.chkOthers.Text = "Others";
            this.chkOthers.UseVisualStyleBackColor = true;
            // 
            // chkWater
            // 
            this.chkWater.AutoSize = true;
            this.chkWater.Location = new System.Drawing.Point(351, 16);
            this.chkWater.Name = "chkWater";
            this.chkWater.Size = new System.Drawing.Size(59, 18);
            this.chkWater.TabIndex = 6;
            this.chkWater.Text = "Water";
            this.chkWater.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(214, 14);
            this.label11.TabIndex = 5;
            this.label11.Text = "Is this an electricity, gas or water bill?";
            // 
            // chkGas
            // 
            this.chkGas.AutoSize = true;
            this.chkGas.Location = new System.Drawing.Point(300, 16);
            this.chkGas.Name = "chkGas";
            this.chkGas.Size = new System.Drawing.Size(47, 18);
            this.chkGas.TabIndex = 4;
            this.chkGas.Text = "Gas";
            this.chkGas.UseVisualStyleBackColor = true;
            // 
            // chkElec
            // 
            this.chkElec.AutoSize = true;
            this.chkElec.Location = new System.Drawing.Point(222, 16);
            this.chkElec.Name = "chkElec";
            this.chkElec.Size = new System.Drawing.Size(78, 18);
            this.chkElec.TabIndex = 3;
            this.chkElec.Text = "Electricity";
            this.chkElec.UseVisualStyleBackColor = true;
            // 
            // name
            // 
            this.name.HeaderText = "name";
            this.name.Name = "name";
            // 
            // act_utility
            // 
            this.act_utility.HeaderText = "utility";
            this.act_utility.Name = "act_utility";
            // 
            // accountno
            // 
            this.accountno.HeaderText = "accountno";
            this.accountno.Name = "accountno";
            this.accountno.ReadOnly = true;
            // 
            // billdate
            // 
            this.billdate.HeaderText = "billdate";
            this.billdate.Name = "billdate";
            this.billdate.ReadOnly = true;
            // 
            // servicefrom
            // 
            this.servicefrom.HeaderText = "servicefrom";
            this.servicefrom.Name = "servicefrom";
            this.servicefrom.ReadOnly = true;
            // 
            // serviceto
            // 
            this.serviceto.HeaderText = "serviceto";
            this.serviceto.Name = "serviceto";
            this.serviceto.ReadOnly = true;
            // 
            // duedate
            // 
            this.duedate.HeaderText = "duedate";
            this.duedate.Name = "duedate";
            this.duedate.ReadOnly = true;
            // 
            // prevamount
            // 
            this.prevamount.HeaderText = "prevamount";
            this.prevamount.Name = "prevamount";
            this.prevamount.ReadOnly = true;
            // 
            // paidamount
            // 
            this.paidamount.HeaderText = "paidamount";
            this.paidamount.Name = "paidamount";
            this.paidamount.ReadOnly = true;
            // 
            // act_currcharge
            // 
            this.act_currcharge.HeaderText = "act_currcharge";
            this.act_currcharge.Name = "act_currcharge";
            this.act_currcharge.ReadOnly = true;
            // 
            // calc_currcharge
            // 
            this.calc_currcharge.HeaderText = "calc_currcharge";
            this.calc_currcharge.Name = "calc_currcharge";
            // 
            // latefee
            // 
            this.latefee.HeaderText = "latefee";
            this.latefee.Name = "latefee";
            this.latefee.ReadOnly = true;
            // 
            // disconnectionfee
            // 
            this.disconnectionfee.HeaderText = "disconnectionfee";
            this.disconnectionfee.Name = "disconnectionfee";
            this.disconnectionfee.ReadOnly = true;
            // 
            // deposit
            // 
            this.deposit.HeaderText = "deposit";
            this.deposit.Name = "deposit";
            // 
            // credit
            // 
            this.credit.HeaderText = "credit";
            this.credit.Name = "credit";
            // 
            // interest
            // 
            this.interest.HeaderText = "interest";
            this.interest.Name = "interest";
            // 
            // other_charges
            // 
            this.other_charges.HeaderText = "other_charges";
            this.other_charges.Name = "other_charges";
            // 
            // amountdue
            // 
            this.amountdue.HeaderText = "amountdue";
            this.amountdue.Name = "amountdue";
            this.amountdue.ReadOnly = true;
            // 
            // calc_amountdue
            // 
            this.calc_amountdue.HeaderText = "calc_amountdue";
            this.calc_amountdue.Name = "calc_amountdue";
            // 
            // finalbill
            // 
            this.finalbill.HeaderText = "finalbill";
            this.finalbill.Name = "finalbill";
            this.finalbill.ReadOnly = true;
            // 
            // svcaddress
            // 
            this.svcaddress.HeaderText = "svcaddress";
            this.svcaddress.Name = "svcaddress";
            // 
            // svcaddresscity
            // 
            this.svcaddresscity.HeaderText = "svcaddresscity";
            this.svcaddresscity.Name = "svcaddresscity";
            // 
            // svcaddressstate
            // 
            this.svcaddressstate.HeaderText = "svcaddressstate";
            this.svcaddressstate.Name = "svcaddressstate";
            // 
            // svcaddresszipcode
            // 
            this.svcaddresszipcode.HeaderText = "svcaddresszipcode";
            this.svcaddresszipcode.Name = "svcaddresszipcode";
            // 
            // property_code
            // 
            this.property_code.HeaderText = "property_code";
            this.property_code.Name = "property_code";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(158, 140);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Expense Management System - PDF Reader";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtf)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.DataGridView dtf;
        private System.Windows.Forms.Button Parse;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDisconFee;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtLateFee;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtPrevBal;
        private System.Windows.Forms.TextBox txtPaymentRcvd;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox txtCurrCharge;
        private System.Windows.Forms.TextBox txtTotalAmt;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox chkTransAmtNo;
        private System.Windows.Forms.CheckBox chkTransAmtYes;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.DateTimePicker dtDueDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dtSvcFr;
        private System.Windows.Forms.DateTimePicker dtBillDate;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DateTimePicker dtSvcTo;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox chbFinalBillNo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox chbFinalBillYes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHouseNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtZipcode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.TextBox txtUSPCity;
        private System.Windows.Forms.TextBox txtUSPState;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtUSPPOBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUSPStreet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUSPZipcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmboBx_USP;
        private System.Windows.Forms.Label lbl_UspOnInvoice;
        private System.Windows.Forms.ComboBox cmbBillName;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtAcctNo;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbOtherUtility;
        private System.Windows.Forms.CheckBox chkOthers;
        private System.Windows.Forms.CheckBox chkWater;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkGas;
        private System.Windows.Forms.CheckBox chkElec;
        private System.Windows.Forms.Button btnGetInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn act_utility;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountno;
        private System.Windows.Forms.DataGridViewTextBoxColumn billdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn servicefrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceto;
        private System.Windows.Forms.DataGridViewTextBoxColumn duedate;
        private System.Windows.Forms.DataGridViewTextBoxColumn prevamount;
        private System.Windows.Forms.DataGridViewTextBoxColumn paidamount;
        private System.Windows.Forms.DataGridViewTextBoxColumn act_currcharge;
        private System.Windows.Forms.DataGridViewTextBoxColumn calc_currcharge;
        private System.Windows.Forms.DataGridViewTextBoxColumn latefee;
        private System.Windows.Forms.DataGridViewTextBoxColumn disconnectionfee;
        private System.Windows.Forms.DataGridViewTextBoxColumn deposit;
        private System.Windows.Forms.DataGridViewTextBoxColumn credit;
        private System.Windows.Forms.DataGridViewTextBoxColumn interest;
        private System.Windows.Forms.DataGridViewTextBoxColumn other_charges;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountdue;
        private System.Windows.Forms.DataGridViewTextBoxColumn calc_amountdue;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalbill;
        private System.Windows.Forms.DataGridViewTextBoxColumn svcaddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn svcaddresscity;
        private System.Windows.Forms.DataGridViewTextBoxColumn svcaddressstate;
        private System.Windows.Forms.DataGridViewTextBoxColumn svcaddresszipcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn property_code;
    }
}

