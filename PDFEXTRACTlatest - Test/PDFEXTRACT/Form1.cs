﻿using ExpertXls.ExcelLib;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Microsoft.VisualBasic;

namespace PDFEXTRACT
{
    public partial class Form1 : Form
    {

        public static string filename, logfile;
        string conn_string = "Data Source=PIV8DBMISNP01;Database=MIS_ALTI;Integrated Security=SSPI;";
        clsConnection cl = new clsConnection();
        string vendorid = "", vendorname = "", vendoradd = "", vendorstrt = "", vendorcity = "", vendorstate = "", vendorzip = "", prop_code = "", client = "", Utility = "", invoicename = "", invoicepath = "", invoiceid = "", clatefee = "";
        DataTable dtInvoice = new DataTable();
        int inv_row = 0;
        string fullpath = string.Format(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\NEW\");
        string fileName;
        bool needToDeleteFile = false;
        string col = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
                        
            timer1.Start();

            filename = "Log_" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".log";
            logfile = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), filename);

            string fullpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\bin\\Debug\\";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //string fullpath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Files\\";

            timer1.Stop();

            //Writelog("Get invoices...");
           
            dtInvoice = GetInvoice();

            //Writelog((dtInvoice.Rows.Count - 1).ToString() + " to be processed...");

            for (inv_row = 0; inv_row <= dtInvoice.Rows.Count - 1; inv_row++)
            {

                foreach (Process clsProcess in Process.GetProcesses())
                {
                    if (clsProcess.ProcessName.Contains("AcroRd32"))
                    {
                        clsProcess.Kill();
                    }
                }

                //Writelog("Copying file...");

                System.IO.File.Copy(dtInvoice.Rows[inv_row]["InvoiceFolderName"].ToString(), @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\" + dtInvoice.Rows[inv_row]["Invoice"] + ".pdf", true);

                fullpath = string.Format(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\");

                invoiceid = dtInvoice.Rows[inv_row]["id"].ToString();
                invoicename = dtInvoice.Rows[inv_row]["Invoice"].ToString();
                invoicepath = dtInvoice.Rows[inv_row]["InvoiceFolderName"].ToString();

                if (Directory.EnumerateFileSystemEntries(fullpath).Any())
                {   

                    string ExecelDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    
                    foreach (var files in Directory.GetFiles(fullpath))
                    {

                        //Writelog("Processing " + dtInvoice.Rows[inv_row]["Invoice"].ToString() + "...");

                        string LatePayment = "0.00";                        
                        FileInfo info = new FileInfo(files);
                        fileName = dtInvoice.Rows[inv_row]["Invoice"].ToString() + ".pdf"; //System.IO.Path.GetFileName(info.FullName);
                        StringBuilder text = new StringBuilder();
                        
                        try
                        {

                            int pwrow = 0;

                            //Writelog("Processing " + dtInvoice.Rows[inv_row]["Invoice"].ToString());

                            if (IsPasswordProtected(fullpath + dtInvoice.Rows[inv_row]["Invoice"].ToString() + ".pdf"))
                            {

                                //Writelog("Getting possible passwords (zip codes)...");

                                //DataTable dtpw = GetPassword(dtInvoice.Rows[inv_row]["invname"].ToString());
                                DataTable dtpw = new DataTable();

                                if (dtInvoice.Rows[inv_row]["accountnum"].ToString() != string.Empty || dtInvoice.Rows[inv_row]["svcaddress_st"].ToString() != string.Empty)
                                {
                                    dtpw = GetPropertyCode( dtInvoice.Rows[inv_row]["svcaddress_st"].ToString(), "", "", "", dtInvoice.Rows[inv_row]["accountnum"].ToString().Replace("x", "").Replace("*", "").Replace("-", "").Replace(" ", ""), dtInvoice.Rows[inv_row]["usp_def_utility"].ToString(), 1, 1);
                                }

                                if (dtpw.Rows.Count == 0)
                                { 
                                    dtpw.Columns.Add("zip_code");
                                    dtpw.Rows.Add("30348"); // Company Billing Zipcode
                                    //dtpw.Rows.Add("8041"); // Company Billing Zipcode
                                }

                                int cval = 0;


                                for (pwrow = 0; pwrow <= dtpw.Rows.Count - 1; pwrow++)
                                {

                                    try
                                    {
                                        
                                        if (IsPasswordValid(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\" + dtInvoice.Rows[inv_row]["Invoice"] + ".pdf",
                                                        System.Text.ASCIIEncoding.ASCII.GetBytes(dtpw.Rows[pwrow]["zip_code"].ToString())))
                                        {

                                            //Writelog("Opening pw protected file...");

                                            if (dtpw.Columns.Count > 1)
                                            {
                                                prop_code = dtpw.Rows[pwrow]["property_code"].ToString();
                                                client = dtpw.Rows[pwrow]["customer_name"].ToString();
                                            }
                                           
                                            byte[] password = System.Text.ASCIIEncoding.ASCII.GetBytes(dtpw.Rows[pwrow]["zip_code"].ToString());
                                            PdfReader.unethicalreading = true;
                                            byte[] p = System.Text.ASCIIEncoding.ASCII.GetBytes("");
                                            PdfReader reader = new PdfReader(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\" + dtInvoice.Rows[inv_row]["Invoice"] + ".pdf", password); 

                                            using (MemoryStream memoryStream = new MemoryStream())
                                            {
                                                using (PdfStamper stamper = new PdfStamper(reader, memoryStream))
                                                {

                                                    stamper.SetEncryption(p, p, PdfWriter.ALLOW_PRINTING,
                                                        PdfWriter.ENCRYPTION_AES_128 | PdfWriter.DO_NOT_ENCRYPT_METADATA);
                                                }

                                                File.WriteAllBytes(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\" + dtInvoice.Rows[inv_row]["Invoice"] + "_OPEN.pdf", memoryStream.ToArray());

                                                reader.Close();

                                                System.IO.File.Copy(@"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\NEW FROM MAILBOX TEST\" + dtInvoice.Rows[inv_row]["Invoice"] + "_OPEN.pdf",
                                                                    dtInvoice.Rows[inv_row]["InvoiceFolderName"].ToString(), true);
                                                System.IO.File.Delete(fullpath + fileName);

                                                fileName = dtInvoice.Rows[inv_row]["Invoice"] + "_OPEN.pdf";

                                                cl.ExecuteQuery("UPDATE tbl_EM_PDFExtEmailData SET svcaddress_zipcode = '" + dtpw.Rows[pwrow]["zip_code"].ToString() + "' WHERE invoicename = '" + dtInvoice.Rows[inv_row]["invname"].ToString() + "'");

                                                DoProcess();

                                                cval = 1;

                                                break;
                                            }
                                        }
                                    }
                                    catch
                                    { }                                                                       
                                }

                                if (cval == 0) // all passwords are not valid
                                {
                                    foreach (Process clsProcess in Process.GetProcesses())
                                    {
                                        if (clsProcess.ProcessName.Contains("AcroRd32"))
                                        {
                                            clsProcess.Kill();
                                        }
                                    }

                                    /* Tag as Unreadable in EMS */

                                    cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, isLatestData = 0, userid = 'pdfreadr'  where id = {0}", dtInvoice.Rows[inv_row]["id"]));


                                    cl.ExecuteQuery(string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                                                   ([Complete]
                                                                   ,[LastDateModified]
                                                                   ,[PendingReasons]
                                                                   ,[Invoice]
                                                                   ,[InvoiceFolderName]
                                                                   ,[userid]
                                                                   ,[isLatestData])
                                                             VALUES
                                                                   (6
                                                                   ,GETDATE()
                                                                   ,'Blank/Unreadable/Cut Invoice'
                                                                   ,'{0}'
                                                                   ,'{1}'
                                                                   ,'pdfreadr'
                                                                   ,1)",
                                                                        dtInvoice.Rows[inv_row]["Invoice"].ToString(),
                                                                        dtInvoice.Rows[inv_row]["InvoiceFolderName"].ToString()));

                                    System.IO.File.Delete(fullpath + fileName);
                                }
                            }
                            else
                            {
                                PdfReader pdfReader = null;

                                try
                                {
                                    pdfReader = new PdfReader(fullpath + dtInvoice.Rows[inv_row]["Invoice"].ToString() + ".pdf");
                                }
                                catch (Exception BadPasswordException)
                                {}

                                if (pdfReader.NumberOfPages > 100)
                                {
                                    DoProcess();
                                }
                                else
                                {

                                    pdfReader.Close();

                                    DoProcess();
                                }                                
                            }

                        }
                        catch (Exception ex)
                        {

                            foreach (Process clsProcess in Process.GetProcesses())
                            {
                                if (clsProcess.ProcessName.Contains("AcroRd32"))
                                {
                                    clsProcess.Kill();
                                }
                            }
                            

                            cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, isLatestData = 0, userid = 'pdfreadr'  where id = {0}", dtInvoice.Rows[inv_row]["id"]));
                            

                            cl.ExecuteQuery(string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                                                   ([Complete]
                                                                   ,[LastDateModified]
                                                                   ,[PendingReasons]
                                                                   ,[Invoice]
                                                                   ,[InvoiceFolderName]
                                                                   ,[userid]
                                                                   ,[isLatestData])
                                                             VALUES
                                                                   (6
                                                                   ,GETDATE()
                                                                   ,'Blank/Unreadable/Cut Invoice'
                                                                   ,'{0}'
                                                                   ,'{1}'
                                                                   ,'pdfreadr'
                                                                    ,1)",
                                                                dtInvoice.Rows[inv_row]["Invoice"].ToString(),
                                                                dtInvoice.Rows[inv_row]["InvoiceFolderName"].ToString()));

                            System.IO.File.Delete(fullpath + fileName);
                        }

                        break;
                    }
                }
            }

            this.Close();
        }

        private void DoProcess()
        {

            DataTable usp_dt = new DataTable();
            DataTable set_dt = new DataTable();
            DataTable dtProperty = new DataTable();
            DataTable val_dt = new DataTable();

            val_dt.Columns.Add("val");

            usp_dt = GetUSPList();

            string FinalBill = "No", USPID = "", temp = "", sval = "", sum_billing = "";
            int dtfr = 0, lrow = 0, a_len = 0;

            PdfReader pdfReader = new PdfReader(fullpath + fileName);

            Uri url = new Uri(fullpath + fileName);
            richTextBox1.Text = string.Empty;

            
                using (pdfReader)
                {
                    //if (pagenum == 0)
                    //{
                        for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                        {
                            string extractedtext = PdfTextExtractor.GetTextFromPage(pdfReader, i);
                            string[] ilines = extractedtext.Split(Environment.NewLine.ToCharArray());

                            richTextBox1.Text += extractedtext;
                        }
                    //}
                    //else
                    //{
                    //    string extractedtext = PdfTextExtractor.GetTextFromPage(pdfReader, pagenum);
                    //    string[] ilines = extractedtext.Split(Environment.NewLine.ToCharArray());

                    //    richTextBox1.Text += extractedtext;
                    //}
                }

            string[] lines = richTextBox1.Text.Split(Environment.NewLine.ToCharArray());
            string lval = "";

            //GET USP ID AND STATE (if any)

            for (int u = 0; u <= lines.Length - 1; u++)
            {
                string myval = lines[u];
                lval += myval.Trim();

                // Check for what USP

                if (USPID == "")
                {
                    for (int usp_row = 0; usp_row <= usp_dt.Rows.Count - 1; usp_row++)
                     {
                         if (myval.ToLower().Contains(usp_dt.Rows[usp_row]["item"].ToString().ToLower())) // USP Name
                        {
                            if (usp_dt.Rows[usp_row]["usp_state"].ToString() != string.Empty) // USP State
                            {
                                if (myval.ToLower().Contains(usp_dt.Rows[usp_row]["usp_state"].ToString().ToLower()))
                                {
                                    USPID = usp_dt.Rows[usp_row]["usp_id"].ToString();
                                    vendorid = usp_dt.Rows[usp_row]["usp_vendor_id"].ToString();
                                    vendorname = usp_dt.Rows[usp_row]["usp_name"].ToString();
                                    vendoradd = usp_dt.Rows[usp_row]["usp_vendor_address"].ToString();
                                    vendorstrt = usp_dt.Rows[usp_row]["Address"].ToString();
                                    vendorcity = usp_dt.Rows[usp_row]["City"].ToString();
                                    vendorstate = usp_dt.Rows[usp_row]["State"].ToString();
                                    vendorzip = usp_dt.Rows[usp_row]["Zipcode"].ToString();
                                    clatefee = usp_dt.Rows[usp_row]["late_fee_op"].ToString();
                                    sum_billing = usp_dt.Rows[usp_row]["summary_billing"].ToString();
                                    break;
                                }
                            }
                            else
                            {
                                USPID = usp_dt.Rows[usp_row]["usp_id"].ToString();
                                vendorid = usp_dt.Rows[usp_row]["usp_vendor_id"].ToString();
                                vendorname = usp_dt.Rows[usp_row]["usp_name"].ToString();
                                vendoradd = usp_dt.Rows[usp_row]["usp_vendor_address"].ToString();
                                vendorstrt = usp_dt.Rows[usp_row]["Address"].ToString();
                                vendorcity = usp_dt.Rows[usp_row]["City"].ToString();
                                vendorstate = usp_dt.Rows[usp_row]["State"].ToString();
                                vendorzip = usp_dt.Rows[usp_row]["Zipcode"].ToString();
                                clatefee = usp_dt.Rows[usp_row]["late_fee_op"].ToString();
                                sum_billing = usp_dt.Rows[usp_row]["summary_billing"].ToString();
                                break;
                            }
                        }
                    }
                }
            }

            if (lval != string.Empty)
            {

                if (dtInvoice.Rows[inv_row]["SourceInvoicePath"].ToString().Contains(@"\PDFReader\") == false)
                {
                    string[] ini = dtInvoice.Rows[inv_row]["SourceInvoicePath"].ToString().Split(Convert.ToChar(@"\"));

                    try
                    {
                        System.IO.File.Copy(fullpath + fileName, @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\EBILL\" + ini[8].ToString(), true);

                        cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set SourceInvoicePath = '" + @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\EBILL\" + ini[8].ToString() + "'  where id = {0}", dtInvoice.Rows[inv_row]["id"]));
                    }
                    catch
                    {

                    }
                }
            }
            else
            {
                string[] ini = dtInvoice.Rows[inv_row]["SourceInvoicePath"].ToString().Split(Convert.ToChar(@"\"));

                try
                {
                    System.IO.File.Copy(fullpath + fileName, @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\Paper bill\" + ini[8].ToString(), true);

                    cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set SourceInvoicePath = '" + @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\Paper bill\" + ini[8].ToString() + "'  where id = {0}", dtInvoice.Rows[inv_row]["id"]));
                }
                catch
                {

                }
            }

            //GET UTILITY /* Check for what utility */

            if (USPID != "")
            {
                SqlConnection uconn = new SqlConnection(conn_string);
                DataTable udt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(string.Format(@"select distinct 'E', map_utility_elec from tbl_EM_PDFExtMapping where map_usp_id = {0} and map_type = 'I' and map_utility_elec is not null
                                                                     union all
                                                                     select distinct 'G', map_utility_gas from tbl_EM_PDFExtMapping where map_usp_id = {0} and map_type = 'I' and map_utility_gas is not null
                                                                     union all
                                                                     select distinct 'W', map_utility_water from tbl_EM_PDFExtMapping where map_usp_id = {0} and map_type = 'I' and map_utility_water is not null",
                                                                     USPID), uconn);

                uconn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(udt);

                uconn.Close();

                for (int u = 0; u <= udt.Rows.Count - 1; u++)
                {
                    for (int v = 0; v <= lines.Length - 1; v++)
                    {
                        string myval = lines[v];

                        if (myval.ToLower().Contains(udt.Rows[u]["map_utility_elec"].ToString().ToLower()))
                        {
                            if (!Utility.Contains(udt.Rows[u][0].ToString()))
                            {
                                Utility = udt.Rows[u][0].ToString();
                            }                           

                            break;
                        }
                    }
                }

                //if (Utility != "")
                //{
                //    Utility = Utility.Substring(0, Utility.Length - 1);
                //}
            }

            if (Utility == "" && USPID != "") /* Get default utility */
            {
                Utility = GetUSPDefUtility(USPID);
            }

            dtf.Rows.Add(fileName, Utility, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

            if (USPID == string.Empty)
            {
                pdfReader.Close();

                foreach (Process clsProcess in Process.GetProcesses())
                {
                    if (clsProcess.ProcessName.Contains("AcroRd32"))
                    {
                        clsProcess.Kill();
                    }
                }

                System.IO.File.Delete(fullpath + fileName);

                cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, userid = 'pdfreadr' where id = {0}", dtInvoice.Rows[inv_row]["id"]));

                dtf.Rows.Clear();
            }


            if (USPID != string.Empty && Utility != string.Empty)
            {

               

                //GET USP MAPPING AND SETUP BY USPID AND FIELD

                for (int f = 2; f <= dtf.Columns.Count - 1; f++) // LOOP FIELD
                {

                    col = dtf.Columns[f].Name;

                    //if (dtf.Columns[f].Name == "property_code" && prop_code != "")
                    //{   
                    //    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = prop_code;

                    //    break;
                    //}

                    if (dtf.Columns[f].Name == "property_code") //&& prop_code == ""
                    {
                        dtProperty = GetPropertyCode((dtf.Rows[dtfr].Cells["svcaddress"].Value == null ? string.Empty : dtf.Rows[dtfr].Cells["svcaddress"].Value.ToString()), 
                                                (dtf.Rows[dtfr].Cells["svcaddresszipcode"].Value == null ? string.Empty : dtf.Rows[dtfr].Cells["svcaddresszipcode"].Value.ToString()),
                                                (dtf.Rows[dtfr].Cells["svcaddresscity"].Value == null ? string.Empty : dtf.Rows[dtfr].Cells["svcaddresscity"].Value.ToString()),
                                                (dtf.Rows[dtfr].Cells["svcaddressstate"].Value == null ? string.Empty : dtf.Rows[dtfr].Cells["svcaddressstate"].Value.ToString()),
                                                dtf.Rows[dtfr].Cells["accountno"].Value.ToString().Replace("-", "").Replace(" ", ""), Utility, 0, 0);

                        
                        if (dtProperty.Rows.Count > 0)
                        {
                            if (prop_code != dtProperty.Rows[0]["property_code"].ToString())
                            {
                                prop_code = dtProperty.Rows[0]["property_code"].ToString();

                                client = dtProperty.Rows[0]["customer_name"].ToString();

                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = prop_code;
                            }
                            else
                            {
                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = prop_code;

                            }
                        }
                        //else
                        //{
                        //    dtf.Rows[dtfr].Cells["svcaddress"].Value = string.Empty;
                        //    dtf.Rows[dtfr].Cells["svcaddresscity"].Value = string.Empty;
                        //    dtf.Rows[dtfr].Cells["svcaddressstate"].Value = string.Empty;
                        //    dtf.Rows[dtfr].Cells["svcaddresszipcode"].Value = string.Empty;
                        //}
                       

                        break;
                    }

                    set_dt = GetUSPSetup(USPID, Utility, dtf.Columns[f].Name.ToString()).DefaultView.ToTable(true, "parseby", "map_" + dtf.Columns[f].Name);

                    int p_row = 0;
                    int arr = 0;
                    string char_split;
                    string chars;
                    string rep;
                    bool ksrc = false;
                    int c = 0;
                    decimal nval = 0;

                    //val_dt.Rows.Clear();

                    for (int s = 0; s <= set_dt.DefaultView.ToTable(true, "map_" + dtf.Columns[f].Name).Rows.Count - 1; s++)
                    {
                        if (IsAmountBreakdown(dtf.Columns[f].Name) && set_dt.Rows[s][1].ToString() != string.Empty && set_dt.Rows.Count > 0)
                        {
                            c += lines.Count(x => x.Contains(set_dt.Rows[s][1].ToString()));
                        }
                        else
                        {
                            c = 1;
                        }
                    }
                     

                    for (int n = 0; n <= c - 1; n++)
                    {
                        for (int p = 0; p <= set_dt.Rows.Count - 1; p++) // PARSE
                        {
                            for (lrow = 0; lrow <= lines.Length - 1; lrow++) // PDF DATA
                            {
                                string myval = lines[lrow];

                                try
                                {

                                    if (set_dt.Rows[p][1].ToString() != string.Empty)
                                    {

                                        if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "d")
                                        {

                                            string dtext = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);

                                            for (int r = 0; r <= lines.Length - 1; r++)
                                            {
                                                if (lines[r].ToString().Contains(dtext))
                                                {
                                                    lines[r] = lines[r].ToString().Replace(dtext, "");
                                                }
                                            }
                                        }

                                        if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "x")
                                        {

                                            string dtext = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);

                                            for (int r = 0; r <= lines.Length - 1; r++)
                                            {
                                                if (lines[r].ToString().Contains(dtext))
                                                {
                                                    lines[r] = lines[r].ToString().Replace(lines[r].ToString(), "");
                                                }
                                            }
                                        }

                                        if (myval.Contains(set_dt.Rows[p][1].ToString()) || myval.Contains(set_dt.Rows[p][1].ToString().Replace(" ", "")))
                                        {

                                            ksrc = true;
                                            //dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = "Yes";

                                            p_row = lrow;

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "1") /* Capture or start from keyword */
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = myval.ToString().Trim();

                                                sval = myval.ToString().Trim();
                                                
                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "8") /* Exact match or Contains text */
                                            {
                                                if (myval.Trim() == set_dt.Rows[p][1].ToString() || (myval.Trim().Contains(set_dt.Rows[p][1].ToString())))
                                                {
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = "True";
                                                    //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                    break;
                                                }
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "4") /* Do Substring */
                                            {
                                                Boolean res = false;

                                                if (IsDateValue(dtf.Columns[f].Name))
                                                {
                                                    try
                                                    {
                                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value);

                                                        res = true;
                                                    }
                                                    catch
                                                    {
                                                        res = false;
                                                    }
                                                }

                                                if (res == false)
                                                {
                                                    int fr = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));
                                                    int len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1));

                                                    if (fr < 0)
                                                    {
                                                        fr = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length + fr;
                                                    }

                                                    if (len < 0)
                                                    {
                                                        len = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length + len;
                                                    }

                                                    if (len < dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length)
                                                    {
                                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Substring(fr, len).Trim();
                                                        //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                        break;
                                                    }
                                                }                                                

                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "3")
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(myval, @"(?<=" + set_dt.Rows[p][1].ToString() + @" ).*").Groups[0].Value.Trim(); ; //Regex.Match(myval, set_dt.Rows[p][1].ToString() + " (.*)").Groups[1].Value;
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "2")
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(myval, string.Format(@"^.*?(?= {0})", set_dt.Rows[p][1].ToString())).Groups[0].Value.Trim();
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }
                                        }

                                    }
                                    else
                                    {

                                        if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "0")
                                        {
                                            p_row = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));

                                            if (lrow == p_row)
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[p_row].ToString().Trim(); //myval.Trim();
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }
                                        }

                                        if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "4") /* Do Substring */
                                        {
                                            int fr = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));
                                            int len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1));

                                            if (fr < 0)
                                            {
                                                fr = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length + fr;
                                            }

                                            if (len < 0)
                                            {
                                                len = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length + len;
                                            }

                                            if (len < dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length)
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Substring(fr, len).Trim();
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }
                                            else
                                            {
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }


                                        }
                                    }


                                    if (lrow == p_row)
                                    {
                                        if (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value != null && dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString() != string.Empty)
                                        {
                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "5")
                                            {
                                                Regex regex = new Regex(" {2,}", RegexOptions.None);

                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = regex.Replace(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), " ");

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());
                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "6") /* Get string by Split */
                                            {
                                                char_split = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);
                                                arr = Convert.ToInt16(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1));
                                                string[] rowdata = null;

                                                if (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Contains(set_dt.Rows[p][1].ToString()))
                                                {
                                                    rowdata = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Substring(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().IndexOf(set_dt.Rows[p][1].ToString()), dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length - dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().IndexOf(set_dt.Rows[p][1].ToString())).ToString().Split(Convert.ToChar(char_split));

                                                    if (arr >= 0)
                                                    {
                                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = rowdata[arr].Trim();
                                                    }
                                                    else
                                                    {
                                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = rowdata[rowdata.Length + arr].Trim();
                                                    }
                                                }
                                                else
                                                {
                                                    rowdata = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim().Split(Convert.ToChar(char_split));

                                                    if (arr >= 0)
                                                    {
                                                        if (arr > rowdata.Length - 1)
                                                        {
                                                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim();
                                                        }
                                                        else
                                                        {
                                                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = rowdata[arr].Trim();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = rowdata[rowdata.Length + arr].Trim();
                                                    }

                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());


                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "9")
                                            {
                                                int len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));

                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), @"\d{" + len + "}").Groups[0].Value.Trim();
                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }


                                            //if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "d")
                                            //{
                                            //    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), @".*\d").Groups[0].Value.Trim(); ;
                                            //    //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                            //}


                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "r") // replace
                                            {
                                                chars = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);
                                                rep = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1);

                                                if (rep == "e")
                                                {
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace(chars, "");
                                                }
                                                else
                                                {
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace(chars, rep).ToString().Trim();
                                                }
                                            }


                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "s")
                                            {

                                                int min = 0;
                                                string sym = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);

                                                while (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim().Contains(sym) == false)
                                                {
                                                    min += 1;
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow - min].ToString().Trim();
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                                break;
                                            }


                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "l")
                                            {
                                                int len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));
                                                int num = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1));

                                                if (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Length <= len)
                                                {
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow + a_len + num];
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "c")
                                            {
                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), @"\d.*").Groups[0].Value.Trim(); //Regex.Match(myval, string.Format(@"^.*?(?= {0})", set_dt.Rows[p][1].ToString())).Groups[0].Value.Trim()

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                                break;
                                            }


                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "b") // before; step back
                                            {

                                                int len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));

                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow - len].Trim();

                                                while (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString() == string.Empty)
                                                {
                                                    len += 1;
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow - len].Trim();
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "a") //after; go forward
                                            {
                                               
                                                a_len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));

                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow + a_len].Trim();

                                                while (dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString() == string.Empty)
                                                {
                                                    a_len += 1;
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = lines[lrow + a_len].Trim();
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }


                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "t") // datetime; get datetime value
                                            {
                                                string dir = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1);
                                                a_len = Convert.ToInt16(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1));
                                                string val = string.Empty;
                                                string[] tval = null;


                                                if (dir == "+")
                                                {
                                                    for (int r = 1; r <= a_len; r++)
                                                    {

                                                        val = lines[lrow + r].Trim();
                                                        tval = val.Split(Convert.ToChar(" "));

                                                        try
                                                        {
                                                            if (val.Length > 8)
                                                            {
                                                                try
                                                                {
                                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(Regex.Match(val, @"\w{3}\s\d{1,2},\s\d{4}").ToString());
                                                                }
                                                                catch
                                                                {
                                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(val);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(val.Substring(0, 6));
                                                            }

                                                            break;
                                                        }
                                                        catch
                                                        {
                                                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = string.Empty;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    for (int r = 1; r <= a_len; r++)
                                                    {

                                                        try
                                                        {
                                                            val = Regex.Match(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), @"\d{1,2}.+").ToString();

                                                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(val);

                                                            break;
                                                        }
                                                        catch
                                                        {
                                                            val = lines[lrow - r].Trim();
                                                            tval = val.Split(Convert.ToChar(" "));

                                                            try
                                                            {
                                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDateTime(val);

                                                                break;
                                                            }
                                                            catch
                                                            {
                                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = string.Empty;
                                                            }
                                                        }
                                                    }
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "p") // Get text through pattern in Regex
                                            {

                                                a_len = Convert.ToInt32(set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("(") + 1, set_dt.Rows[p][0].ToString().IndexOf("^") - set_dt.Rows[p][0].ToString().IndexOf("(") - 1));
                                                string pattern = set_dt.Rows[p][0].ToString().Substring(set_dt.Rows[p][0].ToString().IndexOf("^") + 1, set_dt.Rows[p][0].ToString().IndexOf(")") - set_dt.Rows[p][0].ToString().IndexOf("^") - 1).Replace("<","(").Replace(">", ")").ToString(); 
                                                string val = string.Empty;

                                                if (a_len > 0)
                                                {
                                                    for (int r = 1; r <= a_len; r++)
                                                    {
                                                        val = lines[lrow + r].Trim();

                                                        try
                                                        {
                                                            if (Regex.Match(val, pattern).Length > 0)
                                                            {
                                                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = val;

                                                                break;
                                                            }
                                                        }
                                                        catch
                                                        {
                                                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = string.Empty;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Regex.Match(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString(), pattern);
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Trim());
                                                break;
                                            }

                                            if (set_dt.Rows[p][0].ToString().Substring(0, 1) == "o")
                                            {

                                                if (temp == string.Empty)
                                                {
                                                    temp = dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString();
                                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = string.Empty;
                                                }

                                                //Writelog(dtf.Columns[f].Name + " - " + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString());

                                                break;
                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.ToString());
                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = "!";
                                }
                            }
                        }

                        try
                        {
                            if (IsAmountBreakdown(dtf.Columns[f].Name) && 
                                (Convert.ToDecimal(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace("−", "-").Replace("$", "").Replace("CR", "").Replace("cr", "")) >= 0 ||
                                Convert.ToDecimal(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace("−", "-").Replace("$", "").Replace("CR", "").Replace("cr", "")) < 0))
                            {
                                if (c > 0 && IsAmountBreakdown(dtf.Columns[f].Name) &&
                                dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString() != string.Empty &&
                                val_dt.Select("val = '" + sval.Replace("'","") + "'").Length == 0)
                                {
                                    val_dt.Rows.Add(sval.Replace("'", ""));
                                   

                                    if ((dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().ToUpper().Contains("CR")))
                                    {
                                        dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = Convert.ToDecimal("-" + dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace("−", "-").Replace("$", "").Replace("CR", "").Replace("cr", ""));
                                    }

                                    nval += Convert.ToDecimal(dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value.ToString().Replace("−", "-").Replace("$", ""));
                                    dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = nval.ToString();
                                }

                                if (c > 1)
                                {
                                    lines[lrow] = "";
                                }
                            }
                        }
                        catch
                        {
                            nval += 0;
                            dtf.Rows[dtfr].Cells[dtf.Columns[dtf.Columns[f].Name].Index].Value = nval.ToString();
                            lines[lrow] = "";
                        }
                        
                    }
                }
                
                ConvToNumeric(dtfr);


                try
                {
                    dtf.Rows[0].Cells["billdate"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["billdate"].Value);
                }
                catch
                {
                    dtf.Rows[0].Cells["billdate"].Value = string.Empty;
                }

                try
                {


                    if (Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value) > DateTime.Today)
                    {
                        dtf.Rows[0].Cells["servicefrom"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value.ToString().Substring(0,6) + ", " + (Convert.ToInt16(Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value).Year) - 1).ToString());
                    }
                    else
                    {
                        dtf.Rows[0].Cells["servicefrom"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value);
                    }                    
                }
                catch
                {
                    dtf.Rows[0].Cells["servicefrom"].Value = string.Empty;
                }

                try
                {
                    if (Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value) > DateTime.Today)
                    {
                        dtf.Rows[0].Cells["serviceto"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value.ToString().Substring(0, 6) + ", " + (Convert.ToInt16(Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value).Year) - 1).ToString());
                    }
                    else
                    {
                        dtf.Rows[0].Cells["serviceto"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value);
                    }
                }
                catch
                {
                    dtf.Rows[0].Cells["serviceto"].Value = string.Empty;
                }

                try
                {
                    dtf.Rows[0].Cells["duedate"].Value = Convert.ToDateTime(dtf.Rows[0].Cells["duedate"].Value);
                }
                catch
                {
                    dtf.Rows[0].Cells["duedate"].Value = string.Empty;
                }
            }
            else
            {
                dtf.Rows[dtfr].Cells[0].Value = fileName;

                //MessageBox.Show("Utility not captured!");
            }
                       

            cmbBillName.Text = "ALTISOURCE SINGLE FAMILY, INC.";

            switch (Utility)
            {
                case "E":

                    chkElec.Checked = true;
                    chkGas.Checked = false;
                    chkWater.Checked = false;
                    chkOthers.Checked = false;
                    break;
                case "G":

                    chkElec.Checked = false;
                    chkGas.Checked = true;
                    chkWater.Checked = false;
                    chkOthers.Checked = false;
                    break;
                case "W":

                    chkElec.Checked = false;
                    chkGas.Checked = false;
                    chkWater.Checked = true;
                    chkOthers.Checked = false;
                    break;

            }

            //webBrowser1.Navigate(url);

            //txtAcctNo.Text = dtf.Rows[0].Cells["accountno"].Value.ToString();
            //dtBillDate.Value = (dtf.Rows[0].Cells["billdate"].Value == "" ? DateTime.Now : Convert.ToDateTime(dtf.Rows[0].Cells["billdate"].Value));
            //dtSvcFr.Value = (dtf.Rows[0].Cells["servicefrom"].Value == "" ? DateTime.Now : Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value));
            //dtSvcTo.Value = (dtf.Rows[0].Cells["serviceto"].Value == "" ? DateTime.Now : Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value));
            //dtDueDate.Value = (dtf.Rows[0].Cells["duedate"].Value == "" ? DateTime.Now : Convert.ToDateTime(dtf.Rows[0].Cells["duedate"].Value));
            //txtPrevBal.Text = dtf.Rows[0].Cells["prevamount"].Value.ToString();
            //txtPaymentRcvd.Text = dtf.Rows[0].Cells["paidamount"].Value.ToString().Replace("-", "");
            //txtCurrCharge.Text = (Convert.ToDecimal(dtf.Rows[0].Cells["calc_currcharge"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["deposit"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["credit"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["interest"].Value)).ToString();
            //txtLateFee.Text = dtf.Rows[0].Cells["latefee"].Value.ToString();
            //txtDisconFee.Text = dtf.Rows[0].Cells["disconnectionfee"].Value.ToString();
            //txtTotalAmt.Text = dtf.Rows[0].Cells["amountdue"].Value.ToString();

            //if (dtf.Rows[0].Cells["finalbill"].Value.ToString() == "True")
            //{
            //    chbFinalBillYes.Checked = true;
            //    chbFinalBillNo.Checked = false;
            //}
            //else
            //{
            //    chbFinalBillYes.Checked = false;
            //    chbFinalBillNo.Checked = true;
            //}

            //string[] strt = dtf.Rows[0].Cells["svcaddress"].Value.ToString().Split(Convert.ToChar(" "));

            //txtHouseNo.Text = strt[0].ToString();
            //txtStreet.Text = (dtf.Rows[0].Cells["svcaddress"].Value == null ? "" : dtf.Rows[0].Cells["svcaddress"].Value.ToString());
            //txtCity.Text = (dtf.Rows[0].Cells["svcaddresscity"].Value == null ? "" : dtf.Rows[0].Cells["svcaddresscity"].Value.ToString());
            //txtState.Text = (dtf.Rows[0].Cells["svcaddressstate"].Value == null ? "" : dtf.Rows[0].Cells["svcaddressstate"].Value.ToString());
            //txtZipcode.Text = (dtf.Rows[0].Cells["svcaddresszipcode"].Value == null ? "" : dtf.Rows[0].Cells["svcaddresszipcode"].Value.ToString());

            //cmboBx_USP.Text = vendorname;
            //txtUSPCity.Text = vendorcity;
            //txtUSPStreet.Text = vendorstrt;
            //txtUSPState.Text = vendorstate;
            //txtUSPZipcode.Text = vendorzip;

            pdfReader.Close();

            if (USPID != "" && Utility != "")
            {
                ValidateInvoice();
            }

            richTextBox1.Text = string.Empty;
            temp = string.Empty;

        }


        public static bool IsPasswordProtected(string pdfFullname)
        {
            //Writelog("Checking if the bill is password protected...");

            PdfReader pdfReader = null;

            try
            {
                pdfReader = new PdfReader(pdfFullname);
                pdfReader.Close();

                //Writelog("Bill is not password protected...");

                return false;
            }
            catch (Exception BadPasswordException)
            {
                //pdfReader.Close();

                //Writelog("Bill is password protected...");
                return true;
            }
        }

        private void ConvToNumeric(int xrow)
        {

            decimal prevbal = 0;
            decimal payamt = 0; 
            decimal latefee = 0; 
            decimal discfee = 0;
            decimal deposit = 0;
            decimal credit = 0;
            decimal interest = 0;
            decimal currcharge = 0;
            decimal othercharges = 0;

            try
            {
                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["prevamount"].Value.ToString()))
                {

                    prevbal = Convert.ToDecimal(dtf.Rows[xrow].Cells["prevamount"].Value.ToString().Replace("$", "").Replace("−", "-"));

                    if (dtf.Rows[xrow].Cells["prevamount"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["prevamount"].Value.ToString().Contains("-"))
                    {
                        prevbal = Convert.ToDecimal(dtf.Rows[xrow].Cells["prevamount"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    if (dtf.Rows[xrow].Cells["prevamount"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        prevbal = Convert.ToDecimal(dtf.Rows[xrow].Cells["prevamount"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        prevbal = Convert.ToDecimal("-" + prevbal.ToString());
                        dtf.Rows[xrow].Cells["prevamount"].Value = prevbal;
                    }                    

                    dtf.Rows[xrow].Cells["prevamount"].Value = prevbal;
                }
                else
                {
                    dtf.Rows[xrow].Cells["prevamount"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["paidamount"].Value.ToString()))
                {

                    //payamt = Convert.ToDecimal(dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Replace("$", "").Replace("−", "-"));

                    if (dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Contains("-"))
                    {
                        payamt = Convert.ToDecimal(dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    else if (dtf.Rows[xrow].Cells["paidamount"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        payamt = Convert.ToDecimal(dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        payamt = Convert.ToDecimal("-" + payamt.ToString());
                    }

                    else
                    {
                        payamt = Convert.ToDecimal(dtf.Rows[xrow].Cells["paidamount"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    dtf.Rows[xrow].Cells["paidamount"].Value = payamt;
                    
                }
                else
                {
                    dtf.Rows[xrow].Cells["paidamount"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["latefee"].Value.ToString()))
                {
                    latefee = Convert.ToDecimal(dtf.Rows[xrow].Cells["latefee"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    dtf.Rows[xrow].Cells["latefee"].Value = latefee;
                }
                else
                {
                    dtf.Rows[xrow].Cells["latefee"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["disconnectionfee"].Value.ToString()))
                {
                    discfee = Convert.ToDecimal(dtf.Rows[xrow].Cells["disconnectionfee"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    dtf.Rows[xrow].Cells["disconnectionfee"].Value = discfee;
                }
                else
                {
                    dtf.Rows[xrow].Cells["disconnectionfee"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["deposit"].Value.ToString()))
                {

                    //credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("−", "-"));

                    if (dtf.Rows[xrow].Cells["deposit"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["deposit"].Value.ToString().Contains("-"))
                    {
                        deposit = Convert.ToDecimal(dtf.Rows[xrow].Cells["deposit"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    else if (dtf.Rows[xrow].Cells["deposit"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        deposit = Convert.ToDecimal(dtf.Rows[xrow].Cells["deposit"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        deposit = Convert.ToDecimal("-" + deposit.ToString());
                    }
                    else
                    {
                        deposit = Convert.ToDecimal(dtf.Rows[xrow].Cells["deposit"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    dtf.Rows[xrow].Cells["deposit"].Value = deposit;
                }
                else
                {
                    dtf.Rows[xrow].Cells["deposit"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["credit"].Value.ToString()))
                {

                    //credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("−", "-"));

                    if (dtf.Rows[xrow].Cells["credit"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["credit"].Value.ToString().Contains("-"))
                    {
                        credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }
                    else if (dtf.Rows[xrow].Cells["credit"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        credit = Convert.ToDecimal("-" + credit.ToString());
                    }
                    else
                    {
                        try
                        {
                            credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("−", "-"));
                        }
                       catch
                        {
                            credit = 0;
                        }
                    }

                    dtf.Rows[xrow].Cells["credit"].Value = credit;
                }
                else
                {
                    dtf.Rows[xrow].Cells["credit"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["other_charges"].Value.ToString()))
                {

                    //credit = Convert.ToDecimal(dtf.Rows[xrow].Cells["credit"].Value.ToString().Replace("$", "").Replace("−", "-"));

                    if (dtf.Rows[xrow].Cells["other_charges"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["other_charges"].Value.ToString().Contains("-"))
                    {
                        othercharges = Convert.ToDecimal(dtf.Rows[xrow].Cells["other_charges"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    else if (dtf.Rows[xrow].Cells["other_charges"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        othercharges = Convert.ToDecimal(dtf.Rows[xrow].Cells["other_charges"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        othercharges = Convert.ToDecimal("-" + othercharges.ToString());
                    }
                    else
                    {
                        othercharges = Convert.ToDecimal(dtf.Rows[xrow].Cells["other_charges"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    dtf.Rows[xrow].Cells["other_charges"].Value = othercharges;
                }
                else
                {
                    dtf.Rows[xrow].Cells["other_charges"].Value = 0;
                }



                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["interest"].Value.ToString()))
                {
                    interest = Convert.ToDecimal(dtf.Rows[xrow].Cells["interest"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    dtf.Rows[xrow].Cells["interest"].Value = interest;
                }
                else
                {
                    dtf.Rows[xrow].Cells["interest"].Value = 0;
                }

                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["act_currcharge"].Value.ToString()))
                {
                    currcharge = Convert.ToDecimal(dtf.Rows[xrow].Cells["act_currcharge"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    dtf.Rows[xrow].Cells["act_currcharge"].Value = dtf.Rows[xrow].Cells["act_currcharge"].Value.ToString().Replace("$", "").Replace("−", "-");

                    if (clatefee == "-")
                    {
                        currcharge = currcharge - latefee;
                    }
                }
                else
                {
                    dtf.Rows[xrow].Cells["act_currcharge"].Value = 0;
                }


                if (!String.IsNullOrWhiteSpace(dtf.Rows[xrow].Cells["amountdue"].Value.ToString()))
                {
                    try
                    {
                        dtf.Rows[xrow].Cells["amountdue"].Value = Convert.ToDecimal(dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }
                    catch
                    {
                        dtf.Rows[xrow].Cells["amountdue"].Value = "0";
                    }

                    if (dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Contains("−") || dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Contains("-"))
                    {
                        dtf.Rows[xrow].Cells["amountdue"].Value = Convert.ToDecimal(dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Replace("$", "").Replace("−", "-"));
                    }

                    if (dtf.Rows[xrow].Cells["amountdue"].Value.ToString().ToUpper().Contains("CR"))
                    {
                        dtf.Rows[xrow].Cells["amountdue"].Value = Convert.ToDecimal(dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Replace("$", "").Replace("CR", ""));
                        dtf.Rows[xrow].Cells["amountdue"].Value = Convert.ToDecimal("-" + dtf.Rows[xrow].Cells["amountdue"].Value.ToString());
                    }

                    dtf.Rows[xrow].Cells["amountdue"].Value = Convert.ToDecimal(dtf.Rows[xrow].Cells["amountdue"].Value.ToString().Replace("$", ""));
                }
                else
                {
                    dtf.Rows[xrow].Cells["amountdue"].Value = 0;
                }


                dtf.Rows[xrow].Cells["calc_currcharge"].Value = currcharge.ToString();
            }
            catch
            {

            }
            
        }

        

        private DataTable GetPropertyCode(string streetnum, string zipcode, string city, string state, string accountno, string utility, int acctfrmt, int mode)
        {

            string query = string.Empty;

            query = string.Format(@"EXEC sp_GetPropertyCode '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}", streetnum, city, state, zipcode, accountno, utility, acctfrmt, mode);
            
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }            

            conn.Close();

            return dt;

//            if (dt.Rows.Count > 0)
//            {
//                return dt;
//            }
//            else
//            {
//                dt = new DataTable();
//                query = string.Format(@"select distinct top 1 a.*, b.[abbrev_state] from tbl_REO_Properties a
//                                        left join tbl_EM_ZipcodeStateCity b
//                                        on a.city_name = b.city_name
//                                        where full_address like '{0}%'
//                                        and a.city_name = '{1}'
//                                        order by a.client_hierarchy, a.property_code 
//                                                    ", streetnum, city);

//                da = new SqlDataAdapter(query, conn);

//                conn.Open();

//                da.Fill(dt);

//                conn.Close();

//                if (dt.Rows.Count > 0)
//                {
//                    return dt;
//                }
//                else
//                {
//                    return dt;
//                }
//            }
        }

        private DataTable GetInvoice()
        {
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(@"select distinct a.id, Invoice, InvoiceFolderName, SourceInvoicePath, LastDateModified, substring(b.Item, 0, LEN(b.item) - 28) [invname], c.accountnum, c.svcaddress_st, d.usp_def_utility
                                                    --, c.svcaddress_st,  c.svcaddress_zipcode,
                                                    --c.accountnum, isnull(d.bill_password, '') bill_password, 
                                                    --case when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is not null then c.svcaddress_zipcode 
                                                    --when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is null then '30348' 
                                                    --else d.bill_password end [pw]
                                                    from tbl_ADHOC_EM_Tasks a
                                                    outer apply
                                                    (select distinct top 1 item from dbo.Split(a.SourceInvoicePath, '\') where Item like '%.pdf') b
                                                    left join dbo.tbl_EM_PDFExtEmailData c
                                                    on substring(b.Item, 0, LEN(b.item) - 28) = c.invoicename
                                                    left join tbl_EM_PDFExtUSPList d
                                                    on c.vendorcode = d.usp_vendor_id
                                                    where isLatestData = 1
                                                    and Complete = 19
                                                    order by LastDateModified", conn);
            
            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();           

            return dt;
        }

        private DataTable GetPassword(string name)
        {
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
//            SqlDataAdapter da = new SqlDataAdapter(string.Format(@"select distinct a.id, Invoice, InvoiceFolderName, SourceInvoicePath, LastDateModified, substring(b.Item, 0, LEN(b.item) - 28) [invname]
//                                                    , c.svcaddress_st,  c.svcaddress_zipcode,
//                                                    c.accountnum, isnull(d.bill_password, '') bill_password, 
//                                                    case when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is not null then right('00000' + c.svcaddress_zipcode, 5) 
//                                                    when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is null then '30348' 
//                                                    else d.bill_password end [pw]
//                                                    from tbl_ADHOC_EM_Tasks a
//                                                    outer apply
//                                                    (select distinct top 1 item from dbo.Split(a.SourceInvoicePath, '\') where Item like '%.pdf') b
//                                                    left join dbo.tbl_EM_PDFExtEmailData c
//                                                    on substring(b.Item, 0, LEN(b.item) - 28) = c.invoicename
//                                                    left join tbl_EM_PDFExtUSPList d
//                                                    on c.vendorcode = d.usp_vendor_id
//                                                    where isLatestData = 1
//                                                    and Complete = 19
//                                                    and substring(b.Item, 0, LEN(b.item) - 28) = '{0}'
//                                                    order by LastDateModified", name), conn);

            SqlDataAdapter da = new SqlDataAdapter(string.Format(@"select distinct a.id, Invoice, InvoiceFolderName, SourceInvoicePath, LastDateModified, substring(b.Item, 0, LEN(b.item) - 28) [invname]
                                                    , c.svcaddress_st,  c.svcaddress_zipcode,
                                                    c.accountnum, isnull(d.bill_password, '') bill_password, 
                                                    case when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is not null then right('00000' + c.svcaddress_zipcode, 5) 
                                                    when isnull(d.bill_password, '') = '' and c.svcaddress_zipcode is null then '30348' 
                                                    else d.bill_password end [pw]
                                                    from tbl_ADHOC_EM_Tasks a
                                                    outer apply
                                                    (select distinct top 1 item from dbo.Split(a.SourceInvoicePath, '\') where Item like '%.pdf') b
                                                    left join dbo.tbl_EM_PDFExtEmailData c
                                                    on substring(b.Item, 0, LEN(b.item) - 28) = c.invoicename
                                                    left join tbl_EM_PDFExtUSPList d
                                                    on c.vendorcode = d.usp_vendor_id
                                                    where isLatestData = 1
                                                    and Complete = 19
                                                    and substring(b.Item, 0, LEN(b.item) - 28) = '{0}'
                                                    order by LastDateModified", name), conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            return dt;
        }

        private DataTable GetUSPList()
        {
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT a.usp_id, a.usp_name, a.usp_state, a.usp_vendor_id, RTRIM(c.[Address]) + ', ' + c.City + ', ' + c.[State] + ', ' + c.Zipcode [usp_vendor_address], c.[Address], c.City, c.[State], c.Zipcode, b.item  , isnull(a.late_fee_op, '') late_fee_op, a.summary_billing
                                                    from dbo.tbl_EM_PDFExtUSPList a
                                                    outer apply
                                                    (select distinct top 50 item from dbo.Split(a.usp_othername, ',')) b
                                                    left join vw_VID c
                                                    on a.usp_vendor_id = c.Vendor_ID
                                                    order by a.usp_id desc", conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            return dt;
        }

        private string GetUSPDefUtility(string usp_id)
        {

            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(string.Format(@"select top 1 (select item from dbo.Split(usp_def_utility, ',') where ID = 1) from dbo.tbl_EM_PDFExtUSPList where usp_id = {0}", usp_id), conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
           
        }

        private DataTable GetUSPSetup(string usp_id, string utility, string field)
        {
            
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(string.Format(@"select distinct c.item [parseby], b.map_{1}, c.ID, b.map_id
                                                    from dbo.tbl_EM_PDFExtParsing a
                                                    inner join dbo.tbl_EM_PDFExtMapping b
                                                    on a.ext_usp_id = b.map_usp_id
                                                    and a.ext_utility = '{2}'
                                                    outer apply
                                                    (select distinct top 50 * from dbo.Split(a.ext_{1}, '/')) c
                                                    where ext_usp_id = {0} and ext_utility = '{2}' and a.ext_{1} is not null and b.map_type = 'I'
                                                    order by c.ID asc, b.map_id asc", usp_id, field, utility), conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            return dt;
        }

        public static void Writelog(string message)
        {
            message = DateTime.Now.ToString("MMM dd, yyyy HH:mm:ss") + ": " + message;

            FileStream fs = new FileStream(logfile, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            sw.WriteLine(message);
            Console.WriteLine(message);

            sw.Flush();
            fs.Close();
        }

        private void ValidateInvoice()
        {
            SqlConnection conn = new SqlConnection(conn_string);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(string.Format(@"EXEC sp_EM_ValidateInvoice '{0}', {1}, {2}, '{3}', '{4}', '{5}' ", prop_code, dtf.Rows[0].Cells["amountdue"].Value, dtf.Rows[0].Cells["prevamount"].Value, dtf.Rows[0].Cells["accountno"].Value, dtf.Rows[0].Cells["servicefrom"].Value, dtf.Rows[0].Cells["serviceto"].Value), conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            int res = 0;

            res = cl.ExecuteQuery(string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_PDFExtractData]
                                           ([name]
                                           ,[utility]
                                           ,[accountno]
                                           ,[billdate]
                                           ,[servicefrom]
                                           ,[serviceto]
                                           ,[duedate]
                                           ,[prevamount]
                                           ,[paidamount]
                                           ,[act_currcharge]
                                           ,[calc_currcharge]
                                           ,[latefee]
                                           ,[disconnectionfee]
                                           ,[deposit]
                                           ,[credit]
                                           ,[interest]
                                           ,[othercharges]
                                           ,[amountdue]                                           
                                           ,[finalbill]
                                           ,[svcaddress]
                                           ,[svcaddresscity]
                                           ,[svcaddressstate]
                                           ,[svcaddresszipcode]
                                           ,[property_code]
                                           ,[vendorcode]
                                           ,[dateuploaded]
                                           ,[calc_amount])
                                            VALUES
                                            (
                                            '{0}',
                                            '{1}',
                                            '{2}',
                                            CASE WHEN '{3}' = '' THEN NULL ELSE '{3}' END,
                                            CASE WHEN '{4}' = '' THEN NULL ELSE '{4}' END,
                                            CASE WHEN '{5}' = '' THEN NULL ELSE '{5}' END,
                                            CASE WHEN '{6}' = '' THEN NULL ELSE '{6}' END,
                                            '{7}',
                                            '{8}',
                                            '{9}',
                                            '{10}',
                                            '{11}',
                                            '{12}',
                                            '{13}',
                                            '{14}',
                                            '{15}',
                                            '{16}',
                                            '{17}',
                                            '{18}',
                                            '{19}',
                                            '{20}',
                                            '{21}',
                                            '{22}',
                                            '{23}',
                                            '{24}',
                                            GETDATE(),
                                            '{25}'
                                            )",
                                           dtf.Rows[0].Cells["name"].Value.ToString().Replace("_OPEN", "").Replace(".pdf", ""),
                                           Utility,
                                           dtf.Rows[0].Cells["accountno"].Value.ToString(),
                                           (dtf.Rows[0].Cells["billdate"].Value.ToString() == "" ? string.Empty : dtf.Rows[0].Cells["billdate"].Value.ToString()),
                                           (dtf.Rows[0].Cells["servicefrom"].Value.ToString() == "" ? string.Empty : Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value).ToString("yyyy-MM-dd")),
                                           (dtf.Rows[0].Cells["serviceto"].Value.ToString() == "" ? string.Empty : Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value).ToString("yyyy-MM-dd")),
                                           (dtf.Rows[0].Cells["duedate"].Value.ToString() == "" ? string.Empty : dtf.Rows[0].Cells["duedate"].Value.ToString()),
                                           dtf.Rows[0].Cells["prevamount"].Value.ToString(),
                                           dtf.Rows[0].Cells["paidamount"].Value.ToString(),
                                           dtf.Rows[0].Cells["act_currcharge"].Value.ToString().Replace(",", ""),
                                           dtf.Rows[0].Cells["calc_currcharge"].Value.ToString(),
                                           dtf.Rows[0].Cells["latefee"].Value.ToString(),
                                           dtf.Rows[0].Cells["disconnectionfee"].Value.ToString(),
                                           dtf.Rows[0].Cells["deposit"].Value.ToString(),
                                           dtf.Rows[0].Cells["credit"].Value.ToString(),
                                           dtf.Rows[0].Cells["interest"].Value.ToString(),
                                           dtf.Rows[0].Cells["other_charges"].Value.ToString(),
                                           dtf.Rows[0].Cells["amountdue"].Value.ToString(),
                                           dtf.Rows[0].Cells["finalbill"].Value.ToString(),
                                           (dtf.Rows[0].Cells["svcaddress"].Value == null ? string.Empty : dtf.Rows[0].Cells["svcaddress"].Value.ToString()),
                                           (dtf.Rows[0].Cells["svcaddresscity"].Value == null ? string.Empty : dtf.Rows[0].Cells["svcaddresscity"].Value.ToString()),
                                           (dtf.Rows[0].Cells["svcaddressstate"].Value == null ? string.Empty : dtf.Rows[0].Cells["svcaddressstate"].Value.ToString()),
                                           (dtf.Rows[0].Cells["svcaddresszipcode"].Value == null ? string.Empty : dtf.Rows[0].Cells["svcaddresszipcode"].Value.ToString()),
                                           (dtf.Rows[0].Cells["property_code"].Value == null ? string.Empty : dtf.Rows[0].Cells["property_code"].Value.ToString()),  
                                           vendorid,
                                           dt.Rows[0][2].ToString()
                                           ));

            if (res == 0)
            {
                MessageBox.Show("Data not saved!");

                cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 18, userid = 'pdfreadr', isLatestData = 1 where id = {0}", invoiceid));
            }
            else
            {
                cl.ExecuteQuery(string.Format(@"insert into tbl_ADHOC_EM_Tasks
                                                        (property_code, 
                                                        client_code, 
                                                        usp_name, 
                                                        DueDate, 
                                                        ServiceFrom, 
                                                        ServiceTo, 
                                                        Amount, 
                                                        AccountNumber, 
                                                        Complete, 
                                                        LastDateModified, 
                                                        PendingReasons, 
                                                        Invoice, 
                                                        vendor_group, 
                                                        vendor_address, 
                                                        VendorId, 
                                                        final_bill, 
                                                        InvoiceFolderName, 
                                                        userid, 
                                                        invoice_date, 
                                                        CurrentCharge, 
                                                        PreviousBalance, 
                                                        PaymentReceived, 
                                                        LateFee, 
                                                        DisconnectionFee, 
                                                        isLatestData, 
                                                        isAddressedToASFI, 
                                                        BillTo,
                                                        isSentToExpeditedPayment,
                                                        SpecialInstruction,
                                                        isVendorAssigned,
                                                        dateVendorAssigned,
                                                        VendorAssignedStatus
                                                        )                                                       
                                                        values
                                                        (
                                                        '{0}', 
                                                        '{1}', 
                                                        '{2}', 
                                                        CASE WHEN '{3}' = '' THEN NULL ELSE '{3}' END, 
                                                        CASE WHEN '{4}' = '' THEN NULL ELSE '{4}' END, 
                                                        CASE WHEN '{5}' = '' THEN NULL ELSE '{5}' END,
                                                        '{6}', 
                                                        '{7}', 
                                                        '{8}', 
                                                         {9}, 
                                                        '{10}', 
                                                        '{11}', 
                                                        '{12}', 
                                                        '{13}', 
                                                        '{14}', 
                                                        '{15}', 
                                                        '{16}', 
                                                        '{17}', 
                                                        '{18}', 
                                                        '{19}', 
                                                        '{20}', 
                                                        '{21}', 
                                                        '{22}', 
                                                        '{23}', 
                                                        1, 
                                                        1, 
                                                        'ASFI or c/o ASFI',
                                                        1,
                                                        '{24}',
                                                        0,
                                                        NULL,
                                                        NULL
                                                        )",
                                                            prop_code,
                                                            client,
                                                            vendorname,
                                                            (dtf.Rows[0].Cells["duedate"].Value.ToString() == "" ? string.Empty : Convert.ToDateTime(dtf.Rows[0].Cells["duedate"].Value).ToString("yyyy-MM-dd")),
                                                            (dtf.Rows[0].Cells["servicefrom"].Value.ToString() == "" ? string.Empty : Convert.ToDateTime(dtf.Rows[0].Cells["servicefrom"].Value).ToString("yyyy-MM-dd")),
                                                            (dtf.Rows[0].Cells["serviceto"].Value.ToString() == "" ? string.Empty : Convert.ToDateTime(dtf.Rows[0].Cells["serviceto"].Value).ToString("yyyy-MM-dd")),
                                                            dt.Rows[0][2].ToString(),
                                                            dtf.Rows[0].Cells["accountno"].Value.ToString().Replace("-", "").Replace(" ", ""),
                                                            dt.Rows[0][1].ToString(),
                                                            "GETDATE()",
                                                            dt.Rows[0][0].ToString(),
                                                            invoicename,
                                                            Utility,
                                                            vendoradd,
                                                            vendorid,
                                                            (dtf.Rows[0].Cells["finalbill"].Value.ToString() == "True" ? "Yes" : "No"),
                                                            invoicepath,
                                                            "pdfreadr",
                                                            (dtf.Rows[0].Cells["billdate"].Value.ToString() == "" ? null : dtf.Rows[0].Cells["billdate"].Value.ToString()),
                                                            (Convert.ToDecimal(dtf.Rows[0].Cells["calc_currcharge"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["deposit"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["credit"].Value) + Convert.ToDecimal(dtf.Rows[0].Cells["interest"].Value)).ToString(),
                                                            dtf.Rows[0].Cells["prevamount"].Value.ToString(),
                                                            dtf.Rows[0].Cells["paidamount"].Value.ToString().Replace("-", ""),
                                                            dtf.Rows[0].Cells["latefee"].Value.ToString(),
                                                            dtf.Rows[0].Cells["disconnectionfee"].Value.ToString(),
                                                            dt.Rows[0][3].ToString()
                                                            )
                                                       );

            cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, userid = 'pdfreadr', isLatestData = 0 where id = {0}", invoiceid));
            }

            //cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, userid = 'pdfreadr', isLatestData = 0  where id = {0}", invoiceid));

            //cl.ExecuteQuery(string.Format(@"update tbl_ADHOC_EM_Tasks set Complete = 20, userid = 'pdfreadr', isLatestData = 1 where id = {0}", invoiceid));


            prop_code = string.Empty;
            client = string.Empty;
            Utility = string.Empty;
            vendoradd = string.Empty;
            vendorid = string.Empty;
            vendorstrt = string.Empty;
            vendorcity = string.Empty;
            vendorstate = string.Empty;
            vendorzip = string.Empty;
            clatefee = string.Empty;
            invoiceid = string.Empty;
            invoicename = string.Empty;
            invoicepath = string.Empty;

            //inv_row = 0;

            //MessageBox.Show("Record saved!" + Environment.NewLine + Environment.NewLine + "Queue: " + dt.Rows[0][4].ToString() + Environment.NewLine + "Reason: " + dt.Rows[0][0].ToString(), "EMS - PDF Reader");


            //needToDeleteFile = true;
            //webBrowser1.Navigate("about:blank");


            txtAcctNo.Text = string.Empty;
            dtBillDate.Value = DateTime.Now;
            dtSvcFr.Value = DateTime.Now;
            dtSvcTo.Value = DateTime.Now;
            dtDueDate.Value = DateTime.Now;
            txtPrevBal.Text = string.Empty;
            txtPaymentRcvd.Text = string.Empty;
            txtCurrCharge.Text = string.Empty;
            txtLateFee.Text = string.Empty;
            txtDisconFee.Text = string.Empty;
            txtTotalAmt.Text = string.Empty;

            if (dtf.Rows[0].Cells["finalbill"].Value.ToString() == "True")
            {
                chbFinalBillYes.Checked = true;
                chbFinalBillNo.Checked = false;
            }
            else
            {
                chbFinalBillYes.Checked = false;
                chbFinalBillNo.Checked = true;
            }


            txtHouseNo.Text = string.Empty;
            txtStreet.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            txtZipcode.Text = string.Empty;

            cmboBx_USP.Text = string.Empty;
            txtUSPCity.Text = string.Empty;
            txtUSPStreet.Text = string.Empty;
            txtUSPState.Text = string.Empty; ;
            txtUSPZipcode.Text = string.Empty;

            
             System.IO.File.Delete(fullpath + fileName);


            //dtfr += 1;
            richTextBox1.Clear();
            dtf.Rows.Clear();              
        }



        private void Parse_Click(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }

        private void btnGetInv_Click(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ValidateInvoice();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (needToDeleteFile)
            {

                try
                {
                    File.Delete(fullpath + fileName);
                    needToDeleteFile = false;
                    fileName = "";
                }
                catch
                {
                    File.Delete(fullpath + fileName);
                    needToDeleteFile = false;
                    fileName = "";
                }
                
            }
        }

        public bool IsAmountBreakdown(string field)
        {
            switch (field)
            { 
                //case "prevamount":
                case "paidamount":
                //case "act_currcharge":
                case "latefee":
                case "disconnectionfee":
                //case "credit":
                case "interest":                
                    return true;
                default:
                    return false;
            }            
        }


        public bool IsDateValue(string field)
        {
            switch (field)
            {
                case "billdate":
                case "servicefrom":
                case "serviceto":
                case "duedate":
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsPasswordValid(string pdfFullname, byte[] password)
        {
            PdfReader pdfReader = null;

            try
            {
                pdfReader = new PdfReader(pdfFullname, password);

                pdfReader.Close();
                return true;
            }
            catch (Exception BadPasswordException)
            {
                pdfReader.Close();
                return false;
            }
        }

    }
}
